namespace :remunerations do
    task :pay_remunerations => :environment do
        puts "================================================================= Execution Date = " + (DateTime.now).to_s + " ================================================================="
        puts "################################################################# Paying remunerations of " + (Date.today).to_s + " #################################################################"
        pending_remunerations = Remuneration.where("remuneration_date = ? AND status = ?", Date.today, "pending")
        total_amount = 0
        pending_remunerations.each do |f|
            f.pay
            total_amount += f.value
        end
        puts "################################################################# " + pending_remunerations.size.to_s + " Remunerations Paid #################################################################"
        puts "################################################################# Total Amount = $" + total_amount.to_s + " #################################################################"
    end
end
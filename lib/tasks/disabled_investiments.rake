namespace :investiments do
    task :disable_expired => :environment do
        puts "================================================================= Execution Date = " + (DateTime.now).to_s + " ================================================================="
        puts "################################################################# Disabling Expired Investiments #################################################################"
        expired_investments = Investiment.where(status: :activated).where("expiration_date = ?", Date.today)
        total_amount = 0
        expired_investments.each do |f|
            total_amount += f.accumulated_value
            f.disable
            NotifierExpiredInvestimentsMailer.notify_expired_investiment(f.user, f).deliver_now
            if f.user.user.present?
                NotifierExpiredInvestimentsMailer.notify_consultor_for_expired_investiment(f.user.user, f.user, f).deliver_now
            end
        end
        puts "################################################################# " + expired_investments.size.to_s + " Investiments Disabled " + "################################################################"
        puts "################################################################# Total Amount of Investiments = $" + total_amount.to_s + " #################################################################"
    end
end
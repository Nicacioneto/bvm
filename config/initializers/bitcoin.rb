# frozen_string_literal: true

require 'blockcypher'

def blockcypher
  BlockCypher::Api.new(
    currency: Rails.application.credentials[Rails.env.to_sym][:blockcypher][:currency],
    network: Rails.application.credentials[Rails.env.to_sym][:blockcypher][:network],
    api_token: Rails.application.credentials[Rails.env.to_sym][:blockcypher][:api_token]
  )
end

module IntegerExtend
  Integer.module_eval do |m|
    def to_bitcoin
      (self.to_i / 10**8.to_f).to_s
    end
    def to_satoshi
     (self.to_f * 10**8).to_i
    end
    def to_usdt
      (self.to_i / 10**2.to_f).to_s
    end
    def to_cents
     (self.to_f * 10**2).to_i
    end
    def to_brl
      (self.to_i / 10**2.to_f).to_s
    end
    def to_brl_cents
     (self.to_f * 10**2).to_i
    end
  end
end

module FloatExtend
  Float.module_eval do |m|
    def to_bitcoin
      (self.to_i / 10**8.to_f).to_s
    end
    def to_satoshi
     (self.to_f * 10**8).to_i
    end
    def to_usdt
      (self.to_i / 10**2.to_f).to_s
    end
    def to_cents
     (self.to_f * 10**2).to_i
    end
    def to_brl
      (self.to_i / 10**2.to_f).to_s
    end
    def to_brl_cents
     (self.to_f * 10**2).to_i
    end
  end
end

module StringExtend
  String.module_eval do |m|
    def to_bitcoin
      (self.to_i / 10**8.to_f).to_s
    end
    def to_satoshi
     (self.to_f * 10**8).to_i
    end
    def to_usdt
      (self.to_i / 10**2.to_f).to_s
    end
    def to_cents
     (self.to_f * 10**2).to_i
    end
    def to_brl
      (self.to_i / 10**2.to_f).to_s
    end
    def to_brl_cents
     (self.to_f * 10**2).to_i
    end
  end
end

module BigdecimalExtend
  BigDecimal.module_eval do |m|
    def to_bitcoin
      (self.to_i / 10**8.to_f).to_s
    end
    def to_satoshi
     (self.to_f * 10**8).to_i
    end
    def to_usdt
      (self.to_i / 10**2.to_f).to_s
    end
    def to_cents
     (self.to_f * 10**2).to_i
    end
    def to_brl
      (self.to_i / 10**2.to_f).to_s
    end
    def to_brl_cents
     (self.to_f * 10**2).to_i
    end
  end
end
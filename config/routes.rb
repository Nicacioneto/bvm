# frozen_string_literal: true

Rails.application.routes.draw do
  root to: 'dashboards#index'

  devise_for :users, skip: %i[registrations omniauth_callbacks unlocks], controllers: {
    confirmations: 'devise/users/confirmations',
    # omniauth_callbacks: 'devise/users/omniauth_callbacks',
    passwords: 'devise/users/passwords',
    # registrations: 'devise/users/registrations',
    sessions: 'devise/users/sessions'
  }

  devise_scope :user do
    get '/users/:referral_username/sign_up', to: 'devise/users/registrations#new', as: 'new_indication'
    get '/users/edit', to: 'devise/users/registrations#edit', as: 'edit_user_registration'
    patch '/users', to: 'devise/users/registrations#update', as: 'user_registration'
    put '/users', to: 'devise/users/registrations#update'
    post '/users', to: 'devise/users/registrations#create'
    get '/users/cancel', to: 'devise/users/registrations#cancel', as: 'cancel_user_registration'
    delete '/users', to: 'devise/users/registrations#destroy'
  end

  devise_for :admins, skip: %i[confirmations passwords registrations], controllers: {
    # confirmations: 'devise/admins/confirmations',
    # passwords: 'devise/admins/passwords',
    # registrations: 'devise/admins/registrations',
    sessions: 'devise/admins/sessions'
  }, path: '', path_names: { sign_in: 'nAw9D7t2hCLBJwDjPHnUhZJnSyESpf24cxjfsX6tvzCXHMVEahHf3qwyEg2r5DLy' }

  resources :addresses, except: %i[index show destroy]
  get 'addresses/:id/edit_by_admin' => 'addresses#edit_by_admin', as: 'edit_address_by_admin'

  get 'clients' => 'users#index_clients', as: 'index_clients'
  get 'clients_by_admin/:id' => 'users#index_clients_by_admin', as: 'clients_by_admin'
  get 'users' => 'users#index', as: 'index_users_by_admin'
  get 'users/:id/edit' => 'users#edit', as: 'edit_user_by_admin'
  patch 'users/:id' => 'users#update', as: 'user'

  resources :deposits, except: %i[edit update destroy] 
  match '548d82xh4gg5xt6cy8chvxnm6nmxvk6g' => 'deposits#create_btc', :via => :post
  get 'deposits_by_admin' => 'deposits#index_by_admin', as: 'deposits_by_admin'
  get 'deposits/:user_id/list' => 'deposits#list', as: 'deposits_list'

  resources :investiments, except: %i[destroy]
  get 'investiments_by_admin' => 'investiments#index_by_admin', as: 'investiments_by_admin'
  get 'investiments/activate/:id' => 'investiments#activate', as: 'activate_investiment'
  get 'investiments/disable/:id' => 'investiments#disable', as: 'disable_investiment'
  get 'investiments/:user_id/list' => 'investiments#list', as: 'investiments_list'
  post 'investiments/:user_id' => 'investiments#create_by_admin', as: 'create_investiment_by_admin'
  patch 'investiments/:id/edit_values' => 'investiments#edit_values', as: 'edit_investiment_values'

  resources :remunerations, except: %i[new create destroy show]
  get 'remunerations/pay/:id' => 'remunerations#pay', as: 'pay_remuneration'

  resources :btc_withdraws, except: %i[show update destroy]
  patch 'btc_withdraws/apply_penalty/:id' => 'btc_withdraws#apply_penalty', as: 'apply_penalty'
  get 'btc_withdraws_by_admin' => 'btc_withdraws#index_by_admin', as: 'btc_withdraws_by_admin'
  get 'btc_withdraws/confirm_withdraw/:id' => 'btc_withdraws#confirm_withdraw', as: 'confirm_withdraw'
  get 'btc_withdraws/reverse_withdraw/:id' => 'btc_withdraws#reverse_withdraw', as: 'reverse_withdraw'
  match 'e6157772b13beb9adda4e690f623e742' => 'btc_withdraws#update_confirmations', :via => :post
  get 'btc_withdraws/:user_id/list' => 'btc_withdraws#list', as: 'btc_withdraws_list'
  get 'btc_withdraws/:destination/:token' => 'btc_withdraws#confirm_withdraw_request', as: 'confirm_withdraw_request'
  get 'btc_withdraw/resend_withdraw_mail/:id' => 'btc_withdraws#resend_withdraw_mail', as: 'resend_withdraw_mail'

  get 'launches/:user_id' => 'launches#index', as: 'launches'
  post 'launches/:user_id' => 'launches#create'

  resources :contacts, only: %i[new create]
  patch 'packages/:id' => 'packages#update_remuneration_rate', as: 'package'

  get 'extracts/:user_id' => 'extracts#show', as: 'show_extracts_by_admin'

  get 'reports/financial_report' => 'reports#financial_report', as: 'financial_report'

  get 'uploads/new' => 'uploads#new', as: 'new_upload'

  # post 'uploads/' => 'uploads#create', as: 'uploads'

  #banks
  resources :banks, except: [:show]

  #bank_account
  resources :bank_account_clients, only: [:index, :create, :destroy]
  devise_scope :bank_account_clients do
    get "bank_account_clients_admin/:user_id" => "bank_account_clients#edit_admin", as: "bank_account_clients_admin"
    post "bank_account_clients_admin/:user_id" => "bank_account_clients#create_admin", as: "bank_account_clients_create_admin"
    get "bank_account_clients/new" => "bank_account_clients#new", as: "new_bank_account_client"
  end


end

# frozen_string_literal: true

set :output, error: 'log/cron_error_log.log', standard: 'log/cron_log.log'

every 1.day, at: '10:00 pm' do
  rake 'remunerations:pay_remunerations', environment: 'production'
end

every 1.day, at: '11:00 pm' do
  rake 'investiments:disable_expired', environment: 'production'
end

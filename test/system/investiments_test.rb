require "application_system_test_case"

class InvestimentsTest < ApplicationSystemTestCase
  setup do
    @investiment = investiments(:one)
  end

  test "visiting the index" do
    visit investiments_url
    assert_selector "h1", text: "Investiments"
  end

  test "creating a Investiment" do
    visit investiments_url
    click_on "New Investiment"

    fill_in "Deposit", with: @investiment.deposit_id
    fill_in "Expiration date", with: @investiment.expiration_date
    fill_in "Remuneration rate", with: @investiment.remuneration_rate
    fill_in "Renewal at", with: @investiment.renewal_at
    fill_in "Status", with: @investiment.status
    fill_in "User", with: @investiment.user_id
    fill_in "Value", with: @investiment.value
    click_on "Create Investiment"

    assert_text "Investiment was successfully created"
    click_on "Back"
  end

  test "updating a Investiment" do
    visit investiments_url
    click_on "Edit", match: :first

    fill_in "Deposit", with: @investiment.deposit_id
    fill_in "Expiration date", with: @investiment.expiration_date
    fill_in "Remuneration rate", with: @investiment.remuneration_rate
    fill_in "Renewal at", with: @investiment.renewal_at
    fill_in "Status", with: @investiment.status
    fill_in "User", with: @investiment.user_id
    fill_in "Value", with: @investiment.value
    click_on "Update Investiment"

    assert_text "Investiment was successfully updated"
    click_on "Back"
  end

  test "destroying a Investiment" do
    visit investiments_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Investiment was successfully destroyed"
  end
end

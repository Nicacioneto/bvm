require "application_system_test_case"

class BtcWithdrawsTest < ApplicationSystemTestCase
  setup do
    @btc_withdraw = btc_withdraws(:one)
  end

  test "visiting the index" do
    visit btc_withdraws_url
    assert_selector "h1", text: "Btc Withdraws"
  end

  test "creating a Btc withdraw" do
    visit btc_withdraws_url
    click_on "New Btc Withdraw"

    fill_in "Absorved rate", with: @btc_withdraw.absorved_rate
    fill_in "Amount", with: @btc_withdraw.amount
    fill_in "Confirmations", with: @btc_withdraw.confirmations
    fill_in "Destination", with: @btc_withdraw.destination
    fill_in "Fee", with: @btc_withdraw.fee
    fill_in "Status", with: @btc_withdraw.status
    fill_in "Token", with: @btc_withdraw.token
    fill_in "Transaction hash", with: @btc_withdraw.transaction_hash
    fill_in "User", with: @btc_withdraw.user_id
    click_on "Create Btc withdraw"

    assert_text "Btc withdraw was successfully created"
    click_on "Back"
  end

  test "updating a Btc withdraw" do
    visit btc_withdraws_url
    click_on "Edit", match: :first

    fill_in "Absorved rate", with: @btc_withdraw.absorved_rate
    fill_in "Amount", with: @btc_withdraw.amount
    fill_in "Confirmations", with: @btc_withdraw.confirmations
    fill_in "Destination", with: @btc_withdraw.destination
    fill_in "Fee", with: @btc_withdraw.fee
    fill_in "Status", with: @btc_withdraw.status
    fill_in "Token", with: @btc_withdraw.token
    fill_in "Transaction hash", with: @btc_withdraw.transaction_hash
    fill_in "User", with: @btc_withdraw.user_id
    click_on "Update Btc withdraw"

    assert_text "Btc withdraw was successfully updated"
    click_on "Back"
  end

  test "destroying a Btc withdraw" do
    visit btc_withdraws_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Btc withdraw was successfully destroyed"
  end
end

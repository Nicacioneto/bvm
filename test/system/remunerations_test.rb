require "application_system_test_case"

class RemunerationsTest < ApplicationSystemTestCase
  setup do
    @remuneration = remunerations(:one)
  end

  test "visiting the index" do
    visit remunerations_url
    assert_selector "h1", text: "Remunerations"
  end

  test "creating a Remuneration" do
    visit remunerations_url
    click_on "New Remuneration"

    fill_in "Investiment", with: @remuneration.investiment_id
    fill_in "Number", with: @remuneration.number
    fill_in "Remuneration date", with: @remuneration.remuneration_date
    fill_in "Status", with: @remuneration.status
    fill_in "Value", with: @remuneration.value
    click_on "Create Remuneration"

    assert_text "Remuneration was successfully created"
    click_on "Back"
  end

  test "updating a Remuneration" do
    visit remunerations_url
    click_on "Edit", match: :first

    fill_in "Investiment", with: @remuneration.investiment_id
    fill_in "Number", with: @remuneration.number
    fill_in "Remuneration date", with: @remuneration.remuneration_date
    fill_in "Status", with: @remuneration.status
    fill_in "Value", with: @remuneration.value
    click_on "Update Remuneration"

    assert_text "Remuneration was successfully updated"
    click_on "Back"
  end

  test "destroying a Remuneration" do
    visit remunerations_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Remuneration was successfully destroyed"
  end
end

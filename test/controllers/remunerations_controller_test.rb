require 'test_helper'

class RemunerationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @remuneration = remunerations(:one)
  end

  test "should get index" do
    get remunerations_url
    assert_response :success
  end

  test "should get new" do
    get new_remuneration_url
    assert_response :success
  end

  test "should create remuneration" do
    assert_difference('Remuneration.count') do
      post remunerations_url, params: { remuneration: { investiment_id: @remuneration.investiment_id, number: @remuneration.number, remuneration_date: @remuneration.remuneration_date, status: @remuneration.status, value: @remuneration.value } }
    end

    assert_redirected_to remuneration_url(Remuneration.last)
  end

  test "should show remuneration" do
    get remuneration_url(@remuneration)
    assert_response :success
  end

  test "should get edit" do
    get edit_remuneration_url(@remuneration)
    assert_response :success
  end

  test "should update remuneration" do
    patch remuneration_url(@remuneration), params: { remuneration: { investiment_id: @remuneration.investiment_id, number: @remuneration.number, remuneration_date: @remuneration.remuneration_date, status: @remuneration.status, value: @remuneration.value } }
    assert_redirected_to remuneration_url(@remuneration)
  end

  test "should destroy remuneration" do
    assert_difference('Remuneration.count', -1) do
      delete remuneration_url(@remuneration)
    end

    assert_redirected_to remunerations_url
  end
end

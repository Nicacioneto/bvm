require 'test_helper'

class BtcWithdrawsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @btc_withdraw = btc_withdraws(:one)
  end

  test "should get index" do
    get btc_withdraws_url
    assert_response :success
  end

  test "should get new" do
    get new_btc_withdraw_url
    assert_response :success
  end

  test "should create btc_withdraw" do
    assert_difference('BtcWithdraw.count') do
      post btc_withdraws_url, params: { btc_withdraw: { absorved_rate: @btc_withdraw.absorved_rate, amount: @btc_withdraw.amount, confirmations: @btc_withdraw.confirmations, destination: @btc_withdraw.destination, fee: @btc_withdraw.fee, status: @btc_withdraw.status, token: @btc_withdraw.token, transaction_hash: @btc_withdraw.transaction_hash, user_id: @btc_withdraw.user_id } }
    end

    assert_redirected_to btc_withdraw_url(BtcWithdraw.last)
  end

  test "should show btc_withdraw" do
    get btc_withdraw_url(@btc_withdraw)
    assert_response :success
  end

  test "should get edit" do
    get edit_btc_withdraw_url(@btc_withdraw)
    assert_response :success
  end

  test "should update btc_withdraw" do
    patch btc_withdraw_url(@btc_withdraw), params: { btc_withdraw: { absorved_rate: @btc_withdraw.absorved_rate, amount: @btc_withdraw.amount, confirmations: @btc_withdraw.confirmations, destination: @btc_withdraw.destination, fee: @btc_withdraw.fee, status: @btc_withdraw.status, token: @btc_withdraw.token, transaction_hash: @btc_withdraw.transaction_hash, user_id: @btc_withdraw.user_id } }
    assert_redirected_to btc_withdraw_url(@btc_withdraw)
  end

  test "should destroy btc_withdraw" do
    assert_difference('BtcWithdraw.count', -1) do
      delete btc_withdraw_url(@btc_withdraw)
    end

    assert_redirected_to btc_withdraws_url
  end
end

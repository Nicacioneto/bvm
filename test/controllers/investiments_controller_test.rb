require 'test_helper'

class InvestimentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @investiment = investiments(:one)
  end

  test "should get index" do
    get investiments_url
    assert_response :success
  end

  test "should get new" do
    get new_investiment_url
    assert_response :success
  end

  test "should create investiment" do
    assert_difference('Investiment.count') do
      post investiments_url, params: { investiment: { deposit_id: @investiment.deposit_id, expiration_date: @investiment.expiration_date, remuneration_rate: @investiment.remuneration_rate, renewal_at: @investiment.renewal_at, status: @investiment.status, user_id: @investiment.user_id, value: @investiment.value } }
    end

    assert_redirected_to investiment_url(Investiment.last)
  end

  test "should show investiment" do
    get investiment_url(@investiment)
    assert_response :success
  end

  test "should get edit" do
    get edit_investiment_url(@investiment)
    assert_response :success
  end

  test "should update investiment" do
    patch investiment_url(@investiment), params: { investiment: { deposit_id: @investiment.deposit_id, expiration_date: @investiment.expiration_date, remuneration_rate: @investiment.remuneration_rate, renewal_at: @investiment.renewal_at, status: @investiment.status, user_id: @investiment.user_id, value: @investiment.value } }
    assert_redirected_to investiment_url(@investiment)
  end

  test "should destroy investiment" do
    assert_difference('Investiment.count', -1) do
      delete investiment_url(@investiment)
    end

    assert_redirected_to investiments_url
  end
end

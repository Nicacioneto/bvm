class PackagesController < ApplicationController
  load_and_authorize_resource
  
  def update_remuneration_rate
    Package.first.update(package_params)
    respond_to do |format|
      format.html { redirect_to investiments_by_admin_path, notice: 'Remuneration Rate was successfully updated.' }
    end
  end


  def package_params
    params.require(:package).permit(:remuneration_rate)
  end

end

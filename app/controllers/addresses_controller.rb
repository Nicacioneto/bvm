# frozen_string_literal: true

class AddressesController < ApplicationController
  load_and_authorize_resource
  before_action :set_address, only: %i[edit edit_by_admin update]

  before_action :set_user, only: %i[new]

  # GET /addresses/new
  def new
    @address = Address.new
  end

  # GET /addresses/1/edit
  def edit; end

  def edit_by_admin; end

  # POST /addresses
  # POST /addresses.json
  def create
    @address = Address.new(address_params)
    @address.user = current_user
    respond_to do |format|
      if @address.save
        format.html { redirect_to edit_address_path(@address), notice: 'Address was successfully created.' }
        format.json { render :edit, status: :created, location: @address }
      else
        format.html { render :new }
        format.json { render json: @address.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /addresses/1
  # PATCH/PUT /addresses/1.json
  def update
    if user_signed_in?
      respond_to do |format|
        if @address.update(address_params)
          format.html { redirect_to edit_address_path(@address), notice: 'Address was successfully updated.' }
          format.json { render :edit, status: :ok, location: @address }
        else
          format.html { render :edit }
          format.json { render json: @address.errors, status: :unprocessable_entity }
        end
      end
    else
      respond_to do |format|
        if @address.update(address_params)
          format.html { redirect_to edit_address_by_admin_path(@address), notice: 'Address was successfully updated.' }
          format.json { render :edit_by_admin, status: :ok, location: @address }
        else
          format.html { render :edit_by_admin }
          format.json { render json: @address.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_address
    if user_signed_in?
      @address = current_user.address
    elsif admin_signed_in?
      @address = Address.find(params[:id])
    end
  end

  def set_user
    if user_signed_in?
      @user = current_user
    elsif admin_signed_in?
      @user = User.find(params[:user_id])
    end
   end

  # Never trust parameters from the scary internet, only allow the white list through.
  def address_params
    params.require(:address).permit(:address1, :address2, :city, :zipcode, :state, :country, :user_id)
  end
end

class BanksController < ApplicationController
    load_and_authorize_resource
  
    before_action :set_bank, only: [:edit, :update, :destroy]
  
    # GET /banks
    # GET /banks.json
    def index
      @banks = Bank.all
    end
  
    # GET /banks/new
    def new
      @bank = Bank.new
    end
  
    # POST /banks
    # POST /banks.json
    def create
      @bank = Bank.new(bank_params)
      respond_to do |format|
        if (verify_recaptcha(model: @bank) || Rails.env.development?) && @bank.save
          format.html { redirect_to banks_path, notice: I18n.t("validations.banks.created") }
          format.json { render :show, status: :created, location: @bank }
        else
          format.html { render :new }
          format.json { render json: @bank.errors, status: :unprocessable_entity }
        end
      end
    end
  
    # DELETE /banks/1
    # DELETE /banks/1.json
    def destroy
      if @bank.destroy
        respond_to do |format|
          format.html { redirect_to banks_url, notice: I18n.t("validations.banks.deleted") }
          format.json { head :no_content }
        end
      end
    end
  
    private
  
    # Use callbacks to share common setup or constraints between actions.
    def set_bank
      @bank = Bank.find(params[:id])
    end
  
    # Never trust parameters from the scary internet, only allow the white list through.
    def bank_params
      params.require(:bank).permit(:cod, :name)
    end
  end
  
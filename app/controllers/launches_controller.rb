class LaunchesController < ApplicationController

    before_action :authenticate_admin!, :set_user

    def index
      @launch = Launch.new
      @launches = @user.account.launches
    end

    def create
      @launches = @user.account.launches
      @launch = Launch.new(launch_params)
      @launch.account = @user.account
      respond_to do |format|
        if @launch.save
          format.html { redirect_to launches_path(@user), notice: 'Launch was successfully created.' }
          format.json { render :index, status: :created, location: @launch }
        else
          format.html { render :index }
          format.json { render json: @launch.errors, status: :unprocessable_entity }
        end
      end   
    end

    def set_user
        @user = User.find(params[:user_id])
    end

    private
    def launch_params
      params.require(:launch).permit(:value, :type_of_balance, :observation)
    end
end

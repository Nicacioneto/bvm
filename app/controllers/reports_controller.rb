class ReportsController < ApplicationController
  load_and_authorize_resource
  before_action :set_initial_date, :set_final_date
  def financial_report
    @withdraws = BtcWithdraw.where('status = ? AND created_at > ? AND created_at < ?', 'confirmed', @initial_date, @final_date)

    @withdraws_fee = @withdraws.sum(:fee)
    @withdraws_amount = @withdraws.sum(:amount)
    @withdraws_value = @withdraws.sum(:value)
    @withdraws_fee_value = @withdraws_fee.to_bitcoin.to_f * @withdraws_value


    @deposits = Deposit.where('status = ? AND created_at > ? AND created_at < ?', 'confirmed', @initial_date, @final_date)
    @deposits_amount = @deposits.sum(:amount)
    @redirection_fee = @deposits.sum(:fee)
    @deposits_value = @deposits.sum(:value)
    @deposits_fee_value = @redirection_fee.to_bitcoin.to_f * @deposits_value


  end


  def set_initial_date
    @initial_date_input = params[:initial_date]
    @initial_date_input.blank? ? @initial_date = Time.zone.now.to_date : @initial_date = @initial_date_input.to_date
  end

  def set_final_date
    @final_date_input = params[:final_date]
    @final_date_input.blank? ? @final_date = Time.zone.now.to_date + 1.day : @final_date = @final_date_input.to_date + 1.day
  end

end

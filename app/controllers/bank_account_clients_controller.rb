class BankAccountClientsController < ApplicationController
    load_and_authorize_resource
  
    before_action :set_bank_account_client, only: [:show, :destroy]
    before_action :set_banks, only: [:new, :create, :update, :edit_admin]
    before_action :set_client, only: [:edit_admin, :create_admin]
  
    # GET /bank_account_clients
    # GET /bank_account_clients.json
    def index
      @bank_account_clients = current_user.account.bank_account_clients.where(status: :activated).includes(:bank)
    end
  
    # GET /bank_account_clients/new
    def new
      @bank_account_client = BankAccountClient.new
    end
  
    # POST /bank_account_clients
    # POST /bank_account_clients.json
    def create
      @bank_account_client = BankAccountClient.new(bank_account_client_params.merge(account: current_user.account))
      respond_to do |format|
        if (verify_recaptcha(model: @bank_account_client) || Rails.env.development?) && @bank_account_client.save
          format.html { redirect_to bank_account_clients_path, notice: I18n.t("validations.bank_account_client.created") }
          format.json { render :show, status: :created, location: @bank_account_client }
        else
          format.html {  redirect_to bank_account_clients_path, alert: @bank_account_client.errors.full_messages }
          format.json { render json: @bank_account_client.errors, status: :unprocessable_entity }
        end
      end
    end
  
    # POST /bank_account_clients_admin
    def create_admin
      @bank_account_client = BankAccountClient.new(bank_account_client_params.merge(account: @client.account))
      respond_to do |format|
        if (verify_recaptcha(model: @bank_account_client) || Rails.env.development?) && @bank_account_client.save
          format.html { redirect_to bank_account_clients_admin_path, notice: I18n.t("validations.bank_account_client.created") }
          format.json { render :show, status: :created, location: @bank_account_client }
        else
          format.html { redirect_to bank_account_clients_admin_path, notice: I18n.t("validations.bank_account_client.error") }
          format.json { render json: @bank_account_client.errors, status: :unprocessable_entity }
        end
      end
    end
  
    # DELETE /bank_account_clients/1
    # DELETE /bank_account_clients/1.json
    def destroy
      @bank_account_client.deactivate
      respond_to do |format|
        format.html { redirect_to bank_account_clients_url, notice: I18n.t("validations.bank_account_client.deleted") }
        format.json { head :no_content }
      end
    end
  
    def edit_admin
      @bank_account_clients = BankAccountClient.includes(:bank).where(owner_name: @client.name, status: :activated)
      @bank_account_client = @bank_account_clients.where(account_id: params[:user_id]).first
      if @bank_account_client.nil?
        @bank_account_client = BankAccountClient.new
      else
        @bank_account_client
      end
    end
  
    private
  
    # Use callbacks to share common setup or constraints between actions.
    def set_bank_account_client
      @bank_account_client = BankAccountClient.find(params[:id])
    end
  
    def set_client
      @client = Client.find(params[:user_id])
    end
  
    # Never trust parameters from the scary internet, only allow the white list through.
    def bank_account_client_params
      params.require(:bank_account_client).permit(:bank_id, :agency, :account_number, :digit, :operation, :joint, :bank_account_type)
    end
  
    def set_banks
      @banks = Bank.where(status: :activated).order(:cod).map { |bank| ["#{bank.cod} - #{bank.name}"] }
    end
  end
  
# frozen_string_literal: true

class RemunerationsController < ApplicationController
  load_and_authorize_resource
  before_action :set_remuneration, only: [:pay]

  def pay
    @investiment = @remuneration.investiment
    respond_to do |format|
      if @remuneration.pay
        format.html { redirect_to edit_investiment_path(@investiment), notice: 'Remuneration was successfully paid.' }
        format.json { render 'investiments/edit', status: :ok, location: @investiment }
      else
        format.html { redirect_to edit_investiment_path(@investiment), alert: @remuneration.errors.full_messages.to_sentence }
        format.json { render json: @remuneration.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_remuneration
    @remuneration = Remuneration.find(params[:id])
  end
end

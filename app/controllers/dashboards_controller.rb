# frozen_string_literal: true

class DashboardsController < ApplicationController
  load_and_authorize_resource
  def index
    if user_signed_in?
      @user = current_user
      @account = @user.account
      @investiments = @user.investiments
      @activated_investiments = @user.investiments.where(status: :activated)
      # @withdraws_sum = @user.btc_withdraws.where("status = 'paid' OR status = 'pending'").sum(:value)
      get_all_remunerations
      @commissions = @user.commissions
    elsif admin_signed_in?
      hot_wallet = HotWallet.first
      # @hot_wallet_address = hot_wallet.address
      @hot_wallet_address = ''
      # @hot_wallet_balance = hot_wallet.get_balance
      @hot_wallet_balance = 0
      # @cold_wallet_address = Rails.application.credentials[Rails.env.to_sym][:wallets][:cold_wallet_address]
      @cold_wallet_address = 0
      # @cold_wallet_balance = blockcypher.address_final_balance(@cold_wallet_address)
      @cold_wallet_balance = 0
      @users = User.all
      @users_count = @users.count
      @users_verified_count = @users.where(status: :verified).count
      @users_waiting_count = @users.where(status: :waiting).count
      @users_pending_count = @users.where(status: :pending).count
      @accounts = Account.all
      @available_balance = @accounts.sum(:available_balance)
      @investiments = Investiment.all
      @activated_investiments = @investiments.where(status: :activated)
      @accumulated_balance = @activated_investiments.sum(:accumulated_value)
      @investiments_count = @activated_investiments.count
      @users_with_investiment = @investiments.select(:user_id).size
      @investiment_sum = @activated_investiments.sum(:value)

      @withdraws_sum = BtcWithdraw.sum(:value)
    end
  end

  def get_all_remunerations
    @remunerations = []
    @total_amount_received = 0
    @investiments.each do |investiment|
      investiment.remunerations.where(status: :paid).each do |remuneration|
        @remunerations << remuneration
        @total_amount_received += remuneration.value
      end
    end
  end
end

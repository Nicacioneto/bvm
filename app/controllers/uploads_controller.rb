# frozen_string_literal: true

class UploadsController < ApplicationController
  authorize_resource :class => false
  def new
    redirect_to root_path, notice: t("common.awaiting")
  end

  def create
    presentation_uploaded_file = params[:presentation]
    training_material_uploaded_file = params[:training_material]
    institutional_video_uploaded_file = params[:institutional_video]
    consultor_video_uploaded_file = params[:consultor_video]


    unless presentation_uploaded_file.blank?
      File.open(Rails.root.join('public', 'uploads', 'presentation.pdf'), 'wb') do |file|
        file.write(presentation_uploaded_file.read)
      end
    end

    unless training_material_uploaded_file.blank?
      File.open(Rails.root.join('public', 'uploads', 'training_material.pdf'), 'wb') do |file|
        file.write(training_material_uploaded_file.read)
      end
    end

    unless institutional_video_uploaded_file.blank?
      File.open(Rails.root.join('public', 'uploads', 'institutional_video.mp4'), 'wb') do |file|
        file.write(institutional_video_uploaded_file.read)
      end
    end


    unless consultor_video_uploaded_file.blank?
      File.open(Rails.root.join('public', 'uploads', 'consultor_video.mp4'), 'wb') do |file|
        file.write(consultor_video_uploaded_file.read)
      end
    end

    respond_to do |format|
      if presentation_uploaded_file.blank? && training_material_uploaded_file.blank? && institutional_video_uploaded_file.blank? && consultor_video_uploaded_file.blank?
        format.html { redirect_to new_upload_path, alert: 'Arquivo inválido, por favor, envie um arquivo válido'  }
        format.json { render :new, status: :unprocessable_entity }          
      else
        format.html { redirect_to new_upload_path, notice: 'Arquivo atualizado com sucesso'  }
        format.json { render :new, status: :created }
      end
    end
  end
  
end

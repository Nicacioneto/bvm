class ContactsController < ApplicationController

  def new
    redirect_to root_path, notice: t("common.awaiting")
    @user = current_user
    @contact = Contact.new
  end
  
  def create
    @contact = Contact.new(contracts_params)
    respond_to do |format|
      if verify_recaptcha(model: @contact) && @contact.save
        format.html { redirect_to root_path, notice: t('contacts.successfully_sent') }
      else
        format.html { render :new }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  # Never trust parameters from the scary internet, only allow the white list through.
  def contracts_params
    params.require(:contact).permit(:name, :email, :message)
  end
end

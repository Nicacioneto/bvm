# frozen_string_literal: true

class UsersController < ApplicationController
  load_and_authorize_resource
  before_action :set_user, only: %i[edit update index_clients_by_admin]

  def index
    @users = User.all
  end

  def index_clients
    @user = current_user
    @direct_clients = @user.users.select(:id, :email, :name, :phone, :status, :level, :user_id)
    @link = @user.indication_link
    @indirect_clients = @user.get_indirect_users
  end

  def edit
    @direct_clients = @user.users.select(:id, :email, :name, :phone, :status, :level, :user_id)
    @link = @user.indication_link
    @indirect_clients = @user.get_indirect_users
  end

  def index_clients_by_admin
    @direct_clients = @user.users.select(:id, :email, :name, :phone, :status, :level, :user_id)
    @link = @user.indication_link
    @indirect_clients = @user.get_indirect_users
    @commissions = @user.commissions
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if user_params[:status] == 'blocked'
        @user.soft_delete
      else
        @user.disblock
      end
      if @user.update(user_params)
        format.html { redirect_to edit_user_by_admin_path(@user), notice: 'User was successfully updated.' }
        format.json { render :edit, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    if params[:user][:password].empty?
      params.require(:user).permit(:email, :name, :country, :cpf, :phone, :phone_code, :level, :status, :user_id, :confirmed_at, :username)
    else 
      params.require(:user).permit(:email, :name, :country, :cpf, :phone, :phone_code, :level, :status, :user_id, :password, :confirmed_at, :username) 
    end
  end
end

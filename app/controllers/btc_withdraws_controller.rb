# frozen_string_literal: true

class BtcWithdrawsController < ApplicationController
  load_and_authorize_resource except: [:update_confirmations, :confirm_withdraw_request]
  skip_before_action :verify_authenticity_token, only: [:update_confirmations]

  before_action :set_transaction, only: [:update_confirmations]

  before_action :set_btc_withdraw, only: %i[show confirm_withdraw reverse_withdraw resend_withdraw_mail]

  before_action :set_btc_withdraw_by_hash, only: %i[update_confirmations]

  before_action :set_user, only: %i[list]

  # GET /btc_withdraws
  # GET /btc_withdraws.json
  def index
    redirect_to root_path, notice: t("common.awaiting")
    @btc_withdraws = current_user.btc_withdraws
  end

  def index_by_admin
    redirect_to root_path, notice: t("common.awaiting")

    all_btc_withdraws = BtcWithdraw.all
    if params[:status] == 'pending_email'
      @btc_withdraws = all_btc_withdraws.where(status: :pending_email)
    elsif params[:status] == 'request_created'
      @btc_withdraws = all_btc_withdraws.where(status: :request_created)
    elsif params[:status] == 'confirmed'
      @btc_withdraws = all_btc_withdraws.where(status: :confirmed)
    elsif params[:status] == 'unconfirmed'
      @btc_withdraws = all_btc_withdraws.where(status: :unconfirmed)
    elsif params[:status] == 'canceled'
      @btc_withdraws = all_btc_withdraws.where(status: :canceled)
    elsif params[:status] == 'reversed'
      @btc_withdraws = all_btc_withdraws.where(status: :reversed)
    elsif params[:status] == 'all'
      @btc_withdraws = all_btc_withdraws
    end
  end

  # GET /btc_withdraws/1/edit
  def edit; end

  # GET /btc_withdraws/new
  def new
    redirect_to root_path, notice: t("common.awaiting")
    @btc_withdraw = BtcWithdraw.new
  end

  # POST /btc_withdraws
  # POST /btc_withdraws.json
  def create
    respond_to do |format|
      if current_user.can_withdraw?
        @btc_withdraw = BtcWithdraw.new(btc_withdraw_params)
        @btc_withdraw.user = current_user
        @btc_withdraw.set_attributes
        if verify_recaptcha(model: @btc_withdraw) && @btc_withdraw.save
          send_email_to_confirm(current_user, @btc_withdraw)
          format.html { redirect_to btc_withdraws_path, notice: t('withdrawals.successfully_created') }
          format.json { render :edit, status: :created, location: @btc_withdraw }
        else
          format.html { render :new }
          format.json { render json: @btc_withdraw.errors, status: :unprocessable_entity }
        end
      else
        format.html { redirect_to root_path, alert: t("withdrawals.cant_withdraw.#{current_user.level}") }
      end
    end
  end

  # PATCH/PUT /btc_withdraws/1
  # PATCH/PUT /btc_withdraws/1.json
  def apply_penalty
    respond_to do |format|
      if @btc_withdraw.apply_penalty(params[:btc_withdraw][:penalty_rate])
        format.html { redirect_to edit_btc_withdraw_path(@btc_withdraw), notice: 'Value successfully updated.' }
        format.json { render :edit, status: :ok, location: @btc_withdraw }
      else
        format.html { render :edit }
        format.json { render json: @btc_withdraw.errors, status: :unprocessable_entity }
      end
    end
  end

  def confirm_withdraw
    respond_to do |format|
      @btc_withdraw.withdraw
      if @btc_withdraw.status.unconfirmed?
        format.html { redirect_to edit_btc_withdraw_path(@btc_withdraw), notice: 'Withdraw sucessfully created!' }
        format.json { render json: @btc_withdraw.errors, status: :ok }
      else
        format.html { render :edit }
        format.json { render json: @btc_withdraw.errors, status: :unprocessable_entity }
      end
    end
  end

  def confirm_withdraw_request
    btc_withdraw = BtcWithdraw.find_by(token: params[:token], destination: params[:destination], status: :pending_email)
    respond_to do |format|
      if btc_withdraw && Time.zone.now < (btc_withdraw.updated_at + 3.hours)
        btc_withdraw.update_attribute(:status, :request_created)
        format.html { redirect_to root_path, notice: t("withdrawals.request_successfully_confirmed") }
      else 
        format.html { redirect_to root_path, alert: t("validations.withdrawals.confirm_withdraw_request_error")}
      end
    end
  end

  def resend_withdraw_mail
    @btc_withdraw.update_token
    send_email_to_confirm(current_user, @btc_withdraw) if @btc_withdraw.status.pending_email? && !@btc_withdraw.token.nil?
    respond_to do |format|
      format.html { redirect_to btc_withdraws_path, notice: t("withdrawals.successfuly_sent") }
    end 
  end 

  def update_confirmations
    respond_to do |format|
      format.html { head :ok }
      format.json { head :ok }
    end
    body = request.raw_post
    api_call = JSON(body)

    if @btc_withdraw.update(confirmations: @transaction['confirmations'], status: :confirmed)
      logger.info(@transaction)
    end
  end

  def reverse_withdraw
    respond_to do |format|
      if @btc_withdraw.reverse
        format.html { redirect_to root_path, notice: 'Withdraw successfuly reversed!' }
        format.json { render json: @btc_withdraw.errors, status: :ok }
      else
        format.html { render :edit, alert: I18n.t('validations.coin_withdraw.coin_withdraw_successfully_confirmed') }
        format.json { render json: @btc_withdraw.errors, status: :unprocessable_entity }
      end
    end
  end

  def list
    @btc_withdraws = @user.btc_withdraws
  end

  private

  def set_user
    @user = User.find(params[:user_id])
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_btc_withdraw
    @btc_withdraw = BtcWithdraw.find(params[:id])
  end

  def set_transaction
    @transaction = blockcypher.blockchain_transaction(params[:hash])
  end

  def set_btc_withdraw_by_hash
    @btc_withdraw = BtcWithdraw.where(transaction_hash: @transaction['hash']).first
  end

  def send_email_to_confirm(user, btc_withdraw)
    link_to_confirm = set_link_confirmation(btc_withdraw)
    NotifierWithdrawRequestMailer.request_confirmation(user, btc_withdraw, link_to_confirm).deliver_now
  end

  def set_link_confirmation(btc_withdraw)
    confirm_withdraw_request_url(destination: btc_withdraw.destination, token: btc_withdraw.token)
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def btc_withdraw_params
    params.require(:btc_withdraw).permit(:value, :destination)
  end
end

# frozen_string_literal: true

class InvestimentsController < ApplicationController
  load_and_authorize_resource
  before_action :set_investiment, only: %i[show edit update activate disable edit_values]
  before_action :set_user, only: %i[list create_by_admin]

  def new
    if current_user.account.available_balance > 0
      @investiment = Investiment.new
      @remuneration_rate = Package.first.remuneration_rate
    else
      redirect_to new_deposit_path
    end
  end

  def create
    @investiment = Investiment.new(investiment_params)
    @investiment.user = current_user
    @investiment.remuneration_rate = Package.first.remuneration_rate
    respond_to do |format|
      if verify_recaptcha(model: @investiment) && @investiment.create_with_balance
        format.html { redirect_to investiment_path(@investiment), notice: 'Investiment was successfully created.' }
        format.json { render :show, status: :created, location: @investiment }
      else
        format.html { render :new }
        format.json { render json: @investiment.errors, status: :unprocessable_entity }
      end
    end
  end

  def create_by_admin
    @investiment = Investiment.new(create_by_admin_params)
    @investiment.user = @user
    @investiment.remuneration_rate = Package.first.remuneration_rate
    respond_to do |format|
      if @investiment.create_with_balance_by_admin
        format.html { redirect_to edit_investiment_path(@investiment), notice: 'Investiment was successfully created.' }
        format.json { render :edit, status: :created, location: @investiment }
      else
        format.html { render :list }
        format.json { render json: @investiment.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /investiments
  # GET /investiments.json
  def index
    @investiments = current_user.investiments
  end

  def index_by_admin
    all_investiments = Investiment.all
    if params[:status] == "pending"
      @investiments = all_investiments.where(status: :pending)
    elsif params[:status] == "activated"
      @investiments = all_investiments.where(status: :activated)
    elsif params[:status] == "disabled"
      @investiments = all_investiments.where(status: :disabled)
    elsif params[:status] == "canceled"
      @investiments = all_investiments.where(status: :canceled)
    elsif params[:status] == "all"
      @investiments = all_investiments
    end

  end

  # GET /investiments/1
  # GET /investiments/1.json
  def show
    @remunerations = @investiment.remunerations
   end

  # GET /investiments/1/edit
  def edit
    @remunerations = @investiment.remunerations
    @commissions = @investiment.commissions
  end

  # PATCH/PUT /investiments/1
  # PATCH/PUT /investiments/1.json
  def update
    respond_to do |format|
      if @investiment.update(update_investiment_params)
        format.html { redirect_to edit_investiment_path(@investiment), notice: 'Investiment was successfully updated.' }
        format.json { render :edit, status: :ok, location: @investiment }
      else
        format.html { render :edit }
        format.json { render json: @investiment.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit_values
    respond_to do |format|
      if @investiment.edit_values(edit_values_params[:value], edit_values_params[:accumulated_value])
        format.html { redirect_to edit_investiment_path(@investiment), notice: 'Investiment was successfully updated.' }
        format.json { render :edit, status: :ok, location: @investiment }
      else
        format.html { redirect_to edit_investiment_path(@investiment), alert: 'There was an error with your request, please try again' }
        format.json { render json: @investiment.errors, status: :unprocessable_entity }
      end
    end
  end

  def activate
    respond_to do |format|
      if @investiment.activate
        format.html { redirect_to edit_investiment_path(@investiment), notice: 'Investiment was successfully activated.' }
        format.json { render :edit, status: :ok, location: @investiment }
      else
        format.html { redirect_to edit_investiment_path(@investiment), alert: 'There was an error with your request, please try again' }
        format.json { render json: @investiment.errors, status: :unprocessable_entity }
      end
    end
  end

  def disable
    respond_to do |format|
      if @investiment.remunerations.where(status: :pending).size > 0
        format.html { redirect_to edit_investiment_path(@investiment), alert: "Só é possível desabilitar um investimento após o pagamento de todas as remunerações!" }
      else  
        if @investiment.disable
          format.html { redirect_to edit_investiment_path(@investiment), notice: 'Investiment was successfully disabled.' }
          format.json { render :edit, status: :ok, location: @investiment }
        else
          format.html { redirect_to edit_investiment_path(@investiment) }
          format.json { render json: @investiment.errors, status: :unprocessable_entity }
        end
      end 
    end
  end

  def list
    @investiments = @user.investiments
    @investiment = Investiment.new
  end

  private

  def set_user
    if user_signed_in?
      @user = current_user
    elsif admin_signed_in?
      @user = User.find(params[:user_id])
    end
   end

  # Use callbacks to share common setup or constraints between actions.
  def set_investiment
    @investiment = Investiment.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def investiment_params
    params.require(:investiment).permit(:value)
  end
  
  def create_by_admin_params
    params.require(:investiment).permit(:value, :remuneration_rate)
  end

  def update_investiment_params
    params.require(:investiment).permit(:remuneration_rate)
  end

  def edit_values_params
    params.require(:investiment).permit(:value, :accumulated_value)
  end
end

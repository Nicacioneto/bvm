# frozen_string_literal: true

class DepositsController < ApplicationController
  load_and_authorize_resource except: [:create_btc]

  before_action :set_deposit, only: [:show]

  before_action :set_user, only: %i[list]
  before_action :setup_banks, only: [:new]

  def create
    amount = params[:amount].to_brl_cents
    @brl_deposit = BrlDeposit.new(amount: amount, account_id: current_user.account.id, deposit_form: params[:deposit_form], bank_account_client_id: params[:bank_account_client_id], bank_account_sys_admin_id: params[:bank_account_sys_admin_id], voucher: params[:voucher])
    if (verify_recaptcha(model: @brl_deposit) || Rails.env.development?) && @brl_deposit.save
      successful_deposit_creation
    else
      respond_to do |format|
        format.html { redirect_to root_path, alert: @brl_deposit.errors }
        format.json { render json: @deposit.errors, status: :unprocessable_entity }
      end
    end
  end

  def successful_deposit_creation
    respond_to do |format|
      format.html { redirect_to root_path, notice: I18n.t('validations.deposits.requested') }
      format.json { render :show, status: :created, location: @deposit }
    end
  end
  
  # GET /deposits
  # GET /deposits.json
  def index
    @deposits = current_user.deposits
  end

  def index_by_admin
    redirect_to root_path, notice: t("common.awaiting")
    @deposits = Deposit.all
  end

  def new
    @deposit = Deposit.new
    @bank_account_clients = current_user.account.bank_account_clients
  end

  # GET /deposits/1
  # GET /deposits/1.json
  def show; end

  # POST /deposits
  # POST /deposits.json
  def create_btc
    respond_to do |format|
      format.html { head :ok }
      format.json { head :ok }
    end
    unless Deposit.find_by_transaction_hash(params[:transaction_hash])
      deposit = BtcDeposit.new(deposit_params).default_values
      deposit.save
    end
  end


  def list
    @deposits = @user.deposits
  end

  def setup_banks
    account = if current_user
                current_user.account
              else
                @deposit.account
              end
    set_up_fields(account)
    set_up_bank_admin_select
    set_up_bank_admin_list
  end

  def set_up_fields(account)
    @fields = BankAccountClient.map_banks_by_account(account)
  end

  def set_up_bank_admin_select
    @banks_sys_admin_select = BankAccountSysAdmin.where(status: :activated).includes(:bank).map do |bank_account|
      bank_account_bank_name = bank_account.bank.name
      bank_account_id = bank_account.id
        ["#{bank_account_bank_name} |
        Ag: #{bank_account.agency} |
        Conta: #{bank_account.account_number} -
        #{bank_account.digit} | Op: #{bank_account.operation}", bank_account_id]
    end
  end

  def set_up_bank_admin_list
    @banks_sys_admin_list = BankAccountSysAdmin.where(status: :activated).includes(:bank).map do |bank_account|
      bank_account_bank_name = bank_account.bank.name
        "#{bank_account_bank_name} |
        Ag: #{bank_account.agency} |
        Conta: #{bank_account.account_number} -
        #{bank_account.digit} | Op: #{bank_account.operation}---
        Favorecido: #{bank_account.owner_name} | CNPJ: #{bank_account.cnpj}"
    end
  end

  private

  def set_user
    @user = User.find(params[:user_id])
  end
  # Use callbacks to share common setup or constraints between actions.
  def set_deposit
    @deposit = Deposit.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def btc_deposit_params
    params.permit(:transaction_hash)
  end
end

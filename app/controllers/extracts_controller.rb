class ExtractsController < ApplicationController
  before_action :authenticate_admin!  
  def show
    @user = User.find(params[:user_id])

    btc_withdraws = @user.btc_withdraws
    commissions = @user.commissions
    deposits = @user.deposits
    investiments = @user.investiments
    launches = @user.account.launches
    @operations = deposits + investiments + commissions + btc_withdraws + launches
  end
end

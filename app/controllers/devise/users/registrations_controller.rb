# frozen_string_literal: true

class Devise::Users::RegistrationsController < Devise::RegistrationsController
  prepend_before_action :check_captcha, only: [:create]
  before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  def new
    @referral_user = User.find_by_username(params[:referral_username]) if params[:referral_username]
    super
   end

  # POST /resource
  def create
    user_id = User.find_by_username(sign_up_params[:referral_username]).id
    build_resource(sign_up_params.merge({"user_id" => user_id}).except(:referral_username))
    resource.save
    yield resource if block_given?
    if resource.persisted?
      if resource.active_for_authentication?
        set_flash_message! :notice, :signed_up
        sign_up(resource_name, resource)
        respond_with resource, location: after_sign_up_path_for(resource)
      else
        set_flash_message! :notice, :"signed_up_but_#{resource.inactive_message}"
        expire_data_after_sign_in!
        respond_with resource, location: after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      set_minimum_password_length
      if sign_up_params[:referral_username].present?
        respond_to do |format|
          format.html { redirect_to new_indication_path(sign_up_params[:referral_username]), flash: { error: 'Invalid data! Please, check all fields' } }
          format.json { render status: :unprocessable_entity }
        end
      else
        respond_with resource
      end
    end
  end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  #  Override destroy to soft delete
  # DELETE /resource
  def destroy
    resource.soft_delete
    Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
    set_flash_message :notice, :destroyed
    yield resource if block_given?
    respond_with_navigational(resource) { redirect_to after_sign_out_path_for(resource_name) }
  end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up, keys: %i[email cpf country phone_code phone username referral_username name])
  end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
  def check_captcha
    unless verify_recaptcha
      respond_to do |format|
        format.html { redirect_to new_indication_path(params[:user][:referral_username]), flash: { error: 'Invalid data! Please, check all fields' } }
        format.json { render status: :unprocessable_entity }
      end
    end 
  end
end

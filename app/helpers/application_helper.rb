module ApplicationHelper
  def user_has_address?
    current_user.address.present?
  end
end

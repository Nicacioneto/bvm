module BtcWithdrawsHelper
  def decide_tab_activate(type, type_params)
    if type == type_params
      "nav-link active"
    elsif type_params.nil? && type == "all"
      "nav-link active"
    else
      "nav-link"
    end
  end

  def get_size_filter_content_btc_withdraws(type_params)
    if type_params == "all"
      BtcWithdraw.count
    else
      BtcWithdraw.where(status: type_params).size
    end
  end
end

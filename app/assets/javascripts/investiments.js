$(document).on('turbolinks:load', function () {
    (function ($) {

        'use strict';

        var datatableInit = function () {
            var $table = $('#index_investiments_by_admin');

            var table = $table.dataTable({
                sDom: '<"text-right mb-md"T><"row"<"col-lg-6"l><"col-lg-6"f>><"table-responsive"t>p',
                buttons: ['print', 'excel', 'pdf']
            });

            $('<div />').addClass('dt-buttons mb-2 pb-1 text-right').prependTo('#index_investiments_by_admin_wrapper');

            $table.DataTable().buttons().container().prependTo('#index_investiments_by_admin_wrapper .dt-buttons');

            $('#index_investiments_by_admin_wrapper').find('.btn-secondary').removeClass('btn-secondary').addClass('btn-default');
        };

        $(function () {
            datatableInit();
        });

    }).apply(this, [jQuery]);


    (function ($) {

        'use strict';

        var datatableInit = function () {
            var $table = $('#index_remunerations');

            var table = $table.dataTable({
                sDom: '<"text-right mb-md"T><"row"<"col-lg-6"l><"col-lg-6"f>><"table-responsive"t>p',
                buttons: ['print', 'excel', 'pdf']
            });

            $('<div />').addClass('dt-buttons mb-2 pb-1 text-right').prependTo('#index_remunerations_wrapper');

            $table.DataTable().buttons().container().prependTo('#index_remunerations_wrapper .dt-buttons');

            $('#index_remunerations_wrapper').find('.btn-secondary').removeClass('btn-secondary').addClass('btn-default');
        };

        $(function () {
            datatableInit();
        });

    }).apply(this, [jQuery]);

    (function ($) {

        'use strict';

        var datatableInit = function () {
            var $table = $('#index_commissions');

            var table = $table.dataTable({
                sDom: '<"text-right mb-md"T><"row"<"col-lg-6"l><"col-lg-6"f>><"table-responsive"t>p',
                buttons: ['print', 'excel', 'pdf']
            });

            $('<div />').addClass('dt-buttons mb-2 pb-1 text-right').prependTo('#index_commissions_wrapper');

            $table.DataTable().buttons().container().prependTo('#index_commissions_wrapper .dt-buttons');

            $('#index_commissions_wrapper').find('.btn-secondary').removeClass('btn-secondary').addClass('btn-default');
        };

        $(function () {
            datatableInit();
        });

    }).apply(this, [jQuery]);
})

// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require turbolinks

// ========================== Head Libs ==========================
//= require modernizr/modernizr

// ========================== Vendor ==========================
//= require jquery/jquery
//= require jquery-browser-mobile/jquery.browser.mobile
//= require popper/umd/popper.min
//= require bootstrap/dist/js/bootstrap
//= require bootstrap-datepicker/js/bootstrap-datepicker
//= require common/common
//= require nanoscroller/nanoscroller
//= require magnific-popup/jquery.magnific-popup
//= require jquery-placeholder/jquery.placeholder

// ========================== Specific Page Vendor ==========================
//= require autosize/autosize
//= require select2/js/select2
//= require datatables/media/js/jquery.dataTables.min
//= require datatables/media/js/dataTables.bootstrap4.min
//= require datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min
//= require datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min
//= require datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min
//= require datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min
//= require datatables/extras/TableTools/JSZip-2.5.0/jszip.min
//= require datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min
//= require datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts

// ========================== Theme Base, Components and Settings ========================== 
//= require js/theme

// ========================== Theme Custom ==========================
//= require js/custom

// ========================== Theme Initialization Files ==========================
//= require js/theme.init

//= require_tree .

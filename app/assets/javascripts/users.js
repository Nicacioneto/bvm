$(document).on('turbolinks:load', function () {
    $(document).ready(function() {
        $('#select2').select2();
    });

    (function ($) {

        'use strict';

        var datatableInit = function () {
            var $table = $('#index_users_by_admin');

            var table = $table.dataTable({
                sDom: '<"text-right mb-md"T><"row"<"col-lg-6"l><"col-lg-6"f>><"table-responsive"t>p',
                buttons: ['print', 'excel', 'pdf'],
                "order": [[0, "desc"]]
            });

            $('<div />').addClass('dt-buttons mb-2 pb-1 text-right').prependTo('#index_users_by_admin_wrapper');

            $table.DataTable().buttons().container().prependTo('#index_users_by_admin_wrapper .dt-buttons');

            $('#index_users_by_admin_wrapper').find('.btn-secondary').removeClass('btn-secondary').addClass('btn-default');
        };

        $(function () {
            datatableInit();
        });

    }).apply(this, [jQuery]);


    (function ($) {

        'use strict';

        var datatableInit = function () {
            var $table = $('#index_direct_clients');

            var table = $table.dataTable({
                sDom: '<"text-right mb-md"T><"row"<"col-lg-6"l><"col-lg-6"f>><"table-responsive"t>p',
                buttons: ['print', 'excel', 'pdf']
            });

            $('<div />').addClass('dt-buttons mb-2 pb-1 text-right').prependTo('#index_direct_clients_wrapper');

            $table.DataTable().buttons().container().prependTo('#index_direct_clients_wrapper .dt-buttons');

            $('#index_direct_clients_wrapper').find('.btn-secondary').removeClass('btn-secondary').addClass('btn-default');
        };

        $(function () {
            datatableInit();
        });

    }).apply(this, [jQuery]);

    (function ($) {

        'use strict';

        var datatableInit = function () {
            var $table = $('#index_indirect_clients');

            var table = $table.dataTable({
                sDom: '<"text-right mb-md"T><"row"<"col-lg-6"l><"col-lg-6"f>><"table-responsive"t>p',
                buttons: ['print', 'excel', 'pdf']
            });

            $('<div />').addClass('dt-buttons mb-2 pb-1 text-right').prependTo('#index_indirect_clients_wrapper');

            $table.DataTable().buttons().container().prependTo('#index_indirect_clients_wrapper .dt-buttons');

            $('#index_indirect_clients_wrapper').find('.btn-secondary').removeClass('btn-secondary').addClass('btn-default');
        };

        $(function () {
            datatableInit();
        });

    }).apply(this, [jQuery]);
})
function copy_link() {

    /* Get the text field */
    var copyText = document.getElementById("user_link");
    /* Select the text field */
    copyText.select();

    /* Copy the text inside the text field */
    document.execCommand("copy");

    /* Alert the copied text */
    alert("Copied the text: " + copyText.value);
}
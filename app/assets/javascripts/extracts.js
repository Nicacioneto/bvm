$(document).on('turbolinks:load', function () {
    (function ($) {

        'use strict';

        var datatableInit = function () {
            var $table = $('#index_operations_by_admin');

            var table = $table.dataTable({
                sDom: '<"text-right mb-md"T><"row"<"col-lg-6"l><"col-lg-6"f>><"table-responsive"t>p',
                buttons: ['print', 'excel', 'pdf'],
                "order": [[3, "desc"]]

            });

            $('<div />').addClass('dt-buttons mb-2 pb-1 text-right').prependTo('#index_operations_by_admin_wrapper');

            $table.DataTable().buttons().container().prependTo('#index_operations_by_admin_wrapper .dt-buttons');

            $('#index_operations_by_admin_wrapper').find('.btn-secondary').removeClass('btn-secondary').addClass('btn-default');
        };

        $(function () {
            datatableInit();
        });

    }).apply(this, [jQuery]);
})

$('.close').click(function (event) {
  $('#myModal').fadeOut();
  event.preventDefault();
});

$(document).ready(function () {
  var ls = localStorage.getItem("modal");
  if (!ls) {
    $('#myModal').modal('show');
  }
})

$('#myModal').on('shown.bs.modal', function () {
  localStorage.setItem("modal", false);
});
$(document).on('turbolinks:load', function () {
    setInterval(getWithdrawCotation, 3000);

    (function ($) {

        'use strict';

        var datatableInit = function () {
            var $table = $('#index_withdraws_by_admin');

            var table = $table.dataTable({
                sDom: '<"text-right mb-md"T><"row"<"col-lg-6"l><"col-lg-6"f>><"table-responsive"t>p',
                buttons: ['print', 'excel', 'pdf']
            });

            $('<div />').addClass('dt-buttons mb-2 pb-1 text-right').prependTo('#index_withdraws_by_admin_wrapper');

            $table.DataTable().buttons().container().prependTo('#index_withdraws_by_admin_wrapper .dt-buttons');

            $('#index_withdraws_by_admin_wrapper').find('.btn-secondary').removeClass('btn-secondary').addClass('btn-default');
        };

        $(function () {
            datatableInit();
        });

    }).apply(this, [jQuery]);
})


function getWithdrawCotation() {
    url = 'https://blockchain.info/ticker'
    var cotation = $.getJSON(url, function (result) {
        setCotation(result['USD']['last']);
        // setFeeInfo(result['USD']['last']);
    })
}

// function setFeeInfo(cotation) {
//     fee = (0.0004 * cotation).toFixed(2)
//     document.getElementById('fee_info').innerHTML = 'Taxa de Rede: 0.0004 BTC = $' + fee
// }

function setCotation(cotation) {
    $("#btc_withdraw_btc_cotation").val(cotation);
}

function set_withdrawn_amount() {
    var withdraw_value = document.getElementById('btc_withdraw_value');
    var btc_cotation = document.getElementById('btc_withdraw_btc_cotation');
    var withdraw_amount = document.getElementById('btc_withdraw_amount');
    // var total_withdraw_value = document.getElementById('total_withdraw_value');
    withdraw_amount.value = (withdraw_value.value / btc_cotation.value).toFixed(8)
    // total_withdraw_value.value = (withdraw_amount.value - 0.0004).toFixed(8)
}


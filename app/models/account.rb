# frozen_string_literal: true

class Account < ApplicationRecord
  belongs_to :user
  has_many :launches
  has_many :bank_account_clients
  has_many :deposits


  has_paper_trail
  def credit_available(value)
    final_balance = available_balance + value.abs
    update_attribute(:available_balance, final_balance.round(2))
  end

  def debit_available(value)
    if has_enough_available_funds?(value)
      final_balance = available_balance - value.abs
      update_attribute(:available_balance, final_balance.round(2))
    else
      errors.add(:balance, message: 'Requested value is greater than you own')
      raise ActiveRecord::Rollback
    end
  end

  def has_enough_available_funds?(value)
    available_balance >= value
  end

  def credit_accumulated(value)
    final_balance = accumulated_balance + value.abs
    update_attribute(:accumulated_balance, final_balance.round(2))
  end

  def debit_accumulated(value)
    if has_enough_accumulated_funds?(value)
      final_balance = accumulated_balance - value.abs
      update_attribute(:accumulated_balance, final_balance.round(2))
    else
      errors.add(:balance, message: 'Requested value is greater than you own')
      raise ActiveRecord::Rollback
    end
  end

  def has_enough_accumulated_funds?(value)
    accumulated_balance >= value
  end
end

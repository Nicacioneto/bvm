class BtcDeposit < Deposit

  belongs_to :btc_wallet

  has_one :user, through: :btc_wallet
  has_one :investiment

  after_create :credit
  after_create :send_confirmation_mailler
  validates :transaction_hash, :prev_hash, :amount, :btc_wallet_id, :confirmations, :sender, :status, presence: true
  validates :transaction_hash, uniqueness: true
  validates :value, presence: true
  validates :value, numericality: true
  mount_uploader :deposit_receipt, DepositUploader


  def default_values
    tx_ref = set_tx_ref(transaction_hash)
    if Deposit.find_by_transaction_hash(tx_ref['inputs'].first['prev_hash']).present?
      errors.add(:transaction_hash, 'transaction hash not identified')
      false
    else
      initialize_deposit(tx_ref)
      set_cotation
      set_value
      self
    end
  end

  private

  def set_cotation
    result = JSON.parse(RestClient.get('https://blockchain.info/ticker'))
    self.btc_cotation = (result['USD']['last']).round(2)
  end

  def set_tx_ref(transaction_hash)
    Deposit.transaction do
      blockcypher.blockchain_transaction(transaction_hash)
    end
  end

  def set_sender(addresses)
    if addresses.include?(Rails.application.credentials[Rails.env.to_sym][:wallets][:cold_wallet_address])
      (addresses - [Rails.application.credentials[Rails.env.to_sym][:wallets][:cold_wallet_address]]).first
    else
      errors.add(:transaction_hash, 'transaction hash not identified')
      logger.info(errors.values)
      raise(ActiveRecord::Rollback) && (return false)
    end
  end

  def set_btc_wallet(address)
    BtcWallet.find_by_address(address)
  end

  def initialize_deposit(tx_ref)
    sender = set_sender(tx_ref['addresses'])
    self.transaction_hash = tx_ref['hash']
    self.prev_hash = tx_ref['inputs'].first['prev_hash']
    self.amount = tx_ref['total'] + tx_ref['fees']
    self.fee = tx_ref['fees']
    self.confirmations = tx_ref['confirmations']
    self.sender = sender
    self.btc_wallet = set_btc_wallet(sender)
    self.status = 'confirmed'
  end

  def send_confirmation_mailler
    if Rails.env.production?
      DepositsNotifierMailer.send_confirmation_deposit(self.user, self).deliver_now
    end
  end

  def set_value
    self.value = (amount.to_bitcoin.to_f * btc_cotation).round(2)
  end

  def credit
    user.account.credit_available(value)
  end
end
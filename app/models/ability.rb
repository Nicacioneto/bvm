# frozen_string_literal: true

class Ability
  include CanCan::Ability
  def initialize(current_session)
    if current_session.is_a?(Admin) || current_session.is_a?(User)
      can :index, Dashboard
      if current_session.is_a?(User)
        can %i[new edit create update], Address, user_id: current_session.id
        can %i[new index create], Deposit, account_id: current_session.account.id
        can %i[index create new resend_withdraw_mail], BtcWithdraw, user_id: current_session.id
        can %i[index_clients], User unless current_session.level.basic?
        can %i[new create index show], Investiment, user_id: current_session.id
        can [:new], :upload unless current_session.level.basic? 
        can [:index, :new, :create, :destroy], BankAccountClient, :account_id => current_session.account.id
      elsif current_session.is_a?(Admin)
        can %i[edit_by_admin update], Address
        can :manage, User
        can %i[show index_by_admin list], Deposit
        can %i[edit apply_penalty index_by_admin confirm_withdraw reverse_withdraw list], BtcWithdraw
        can %i[show index_by_admin edit update activate disable list create_by_admin edit_values], Investiment
        can %i[pay], Remuneration
        can %i[update_remuneration_rate], Package
        can :manage, Report
        can [:new, :create], :upload 
      end
    else
      CanCan::AccessDenied
    end
  end
end

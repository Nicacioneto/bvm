class HotWallet < ApplicationRecord
  after_create :create_webhook

  def get_balance
    blockcypher.address_final_balance(self.address)
  end

  def create_webhook
    blockcypher.event_webhook_subscribe(Rails.application.credentials[Rails.env.to_sym][:host_path] + "/e6157772b13beb9adda4e690f623e742", "confirmed-tx", address: self.address)
  end
end

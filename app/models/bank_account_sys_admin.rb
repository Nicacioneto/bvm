class BankAccountSysAdmin < ApplicationRecord
    belongs_to :bank
    validates :bank_id, :presence => {message: I18n.t("validations.cant_be_blank", attribute_name: "Bank ID")}


    def bank_id=(value)
        if value.is_a?(Integer)
          write_attribute(:bank_id, value)
        elsif value.is_a?(String) && !Bank.find_by_cod(value.to_i).blank?
          write_attribute(:bank_id, Bank.find_by_cod(value.to_i).id)
        end
    end
    
    def deactivate
        self.update_attribute(:status, :deactivated)
    end
end
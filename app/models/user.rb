# frozen_string_literal: true

class User < ApplicationRecord
  extend Enumerize

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable, :timeoutable, :trackable, :omniauthable, authentication_keys: [:login]

  mount_uploader :avatar, AvatarUploader
  has_one :account
  has_one :address
  has_one :btc_wallet
  has_many :deposits, through: :btc_wallet
  has_many :deposits, through: :account

  has_many :investiments
  has_many :remunerations, through: :investiments
  has_many :commissions
  has_many :btc_withdraws
  has_many :users, dependent: :destroy
  belongs_to :user, optional: true
  attr_writer :login
  validates_uniqueness_of :username
  # Only allow letter, number, underscore and punctuation.
  validates_format_of :username, with: /^[a-zA-Z0-9_\.]*$/, multiline: true
  validates :username, length: { in: 3..30 }

  # phone
  # validates :phone, length: { minimum: 5, maximum: 15 }, on: :update
  # validates :phone, numericality: true, on: :update
  validates :cpf, uniqueness: true, on: :create, if: :is_brazilian?
  # validates :cpf, presence: true, on: :create, if: :is_brazilian?
  validates_cpf_format_of :cpf, if: :is_brazilian?

  # status
  enumerize :status, in: %i[pending waiting verified blocked], default: :pending

  # level
  enumerize :level, in: %i[basic ib_one ib_two ib_three franchise], default: :basic

  after_create :create_account

  # instead of deleting, indicate the user requested a delete & timestamp it
  def soft_delete
    update_attribute(:deleted_at, Time.current)
    update_attribute(:status, :blocked)
  end

  # ensure user account is active
  def active_for_authentication?
    super && !deleted_at
  end

  # provide a custom message for a deleted account
  def inactive_message
    !deleted_at ? super : :deleted_account
  end

  def disblock
    update_attribute(:deleted_at, nil)
  end

  def indication_link
    Rails.application.credentials[Rails.env.to_sym][:host_path] + '/users/' + username + '/sign_up'
  end

  def get_invested_value
    investiments.where(status: :activated).sum(:value)
  end

  def get_direct_invested_value
    direct_investiments_value = 0
    direct_users = users
    direct_users.each do |f|
      investiments = f.investiments.where(status: :activated)
      investiments.each do |i|
        direct_investiments_value += i.value
      end
    end
    direct_investiments_value
  end

  def get_indirect_invested_value
    indirect_investiments_value = 0
    indirect_users = get_indirect_users
    indirect_users.each do |f|
      investiments = f.investiments.where(status: :activated)
      investiments.each do |i|
        indirect_investiments_value += i.value
      end
    end
    indirect_investiments_value
  end

  def get_indirect_users
    direct_users = users.where.not(level: :basic)
    indirect_users = []
    direct_users.each do |f|
      indirect_users << f.users
    end
    response = []
    indirect_users.each do |f|
      response += f.select(:id, :email, :name, :phone, :status, :level, :user_id)
    end
    response
  end

  def send_confirmation_notification?
    if Rails.env.development?
      false
    else
      true
    end
  end

  def update_consultor_level
    current_level = level
    unless level.franchise?
      direct_investiments_value = get_direct_invested_value
      if direct_investiments_value <= 10_000
        update_attribute(:level, :ib_one)
      elsif direct_investiments_value > 10_000 && direct_investiments_value <= 25_000
        update_attribute(:level, :ib_two)
      elsif direct_investiments_value > 25_000
        update_attribute(:level, :ib_three)
      end
      if level != current_level
        NotifierConsultorLevelUpdateMailer.notify_level_update(self).deliver_now
      end
    end
  end

  def consultor_percentage
    if level == :ib_one
      '1%'
    elsif level == :ib_two
      '2%'
    elsif level == :ib_three
      '3%'
    end
  end

  def login
    @login || username || email
  end

  # database authentication
  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_h).where(['lower(username) = :value OR lower(email) = :value', { value: login.downcase }]).first
    elsif conditions.key?(:username) || conditions.key?(:email)
      where(conditions.to_h).first
    end
  end

  def self.tree_data
    output = []
    User.roots.each do |user|
      output << data(user)
    end
    output.to_json
  end

  def self.data(user)
    children = []
    unless user.users.blank?
      user.users.each do |emp|
        children << data(emp)
      end
    end
    if children.empty?
      {name: user.username, title: I18n.t("clients.level_options.#{user.level}")}
    else
      {name: user.username, title: I18n.t("clients.level_options.#{user.level}"), children: children}
    end
  end

  def can_withdraw?
    result = true
    if self.level.ib_one? 
      result = get_invested_value >= 250
    elsif self.level.ib_two?
      result = get_invested_value >= 500 
    elsif self.level.ib_three?
      result = get_invested_value >= 1000 
    end
    result
  end

  private

  def is_brazilian?
    country == 'Brazil'
  end

  def create_btc_wallet
    BtcWallet.create(user_id: id)
  end

  def create_account
    Account.create(user_id: id)
  end
end

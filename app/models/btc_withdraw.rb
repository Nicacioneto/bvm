# frozen_string_literal: true

class BtcWithdraw < ApplicationRecord
  extend Enumerize

  NETWORK_FEE = 40_000
  MINIMAL_WITHDRAW = 150.0
  AVAILABLE = true

  belongs_to :user
  has_one :btc_wallet, through: :user
  validates :amount, presence: true
  validates :amount, numericality: { greater_than_or_equal_to: NETWORK_FEE }
  validates :final_amount, presence: true
  validates :value, presence: true
  validates :value, numericality: { greater_than_or_equal_to: MINIMAL_WITHDRAW }
  validates :destination, presence: true
  validate :valid_address?, on: :create, if: -> { Rails.env.production? }

  enumerize :status, in: %i[pending_email request_created confirmed unconfirmed canceled reversed], default: :pending_email
  enumerize :penalty_rate, in: { '5%': 5, '10%': 10, '15%': 15, '20%': 20, '25%': 25, '30%': 30,
                                 '35%': 35, '40%': 40, '45%': 45, '50%': 50 }
  validate :available
  after_create :debit

  def set_attributes
    set_cotation
    set_amount
    create_unique_token
   end

  def set_cotation
    result = JSON.parse(RestClient.get('https://blockchain.info/ticker'))
    self.btc_cotation = (result['USD']['last']).round(2)
  end

  def set_amount
    amount = value.round(2) / btc_cotation
    self.amount = amount.to_satoshi
    # self.final_amount = amount.to_satoshi - NETWORK_FEE
    self.final_amount = amount.to_satoshi
  end

  def apply_penalty(penalty_rate)
    penalty_value = value * (penalty_rate.to_f / 100)
    penalty_amount = final_amount * (penalty_rate.to_f / 100)
    updated_final_amount = final_amount - penalty_amount
    update(penalty_value: penalty_value, penalty_rate: penalty_rate, penalty_amount: penalty_amount, final_amount: updated_final_amount)
  end

  def withdraw
    total_amount = final_amount.abs
    ActiveRecord::Base.transaction do
      raise ActiveRecord::Rollback unless
      @transaction = blockcypher.send_money(Rails.application.credentials[Rails.env.to_sym][:wallets][:hot_wallet_address],
                                            destination, total_amount.abs, Rails.application.credentials[Rails.env.to_sym][:wallets][:hot_wallet_private])
    end
    if @transaction
      transaction_params = {
        transaction_hash: @transaction['tx']['hash'],
        status: :unconfirmed,
        confirmations: @transaction['tx']['confirmations'],
        fee: @transaction['tx']['fees'],
        # absorved_rate: get_absorved_rate,
        token: nil
      }
      update(transaction_params)
    end
  rescue BlockCypher::Api::Error => e
    logger.info(e)
    errors.add(:destination, I18n.t('validations.withdrawals.transaction_error'))
    update_attributes(status: :request_created, token: nil)
  end

  def debit
    if user.account.has_enough_available_funds?(value)
      user.account.debit_available(value)
    else
      errors.add(:amount, I18n.t('validations.withdrawals.insufficient_funds'))
      raise ActiveRecord::Rollback
      false
    end
  end

  def reverse
    if (status.pending_email? || status.request_created?) && transaction_hash.nil?
      user.account.credit_available(value)
      update_attribute(:status, :reversed)
    else
      errors.add(:amount, I18n.t('validations.withdrawals.already_maked'))
      false
    end
  end

  def admin_confirm_withdraw
    confirmations = blockcypher.blockchain_transaction(transaction_hash)
    if confirmations['confirmations'] > 0
      update_attribute(:status, :confirmed)
    else
      errors.add(:status, I18n.t('validations.withdrawals.successfully_confirmed'))
      false
    end
  end

  def update_token
    create_unique_token
    update_attribute(:token, token)
  end

  private

  def valid_address?
    unless Bitcoin.valid_address? destination
      errors.add(:destination, I18n.t('validations.withdrawals.invalid_address'))
      false
    end
  end

  # def get_absorved_rate
  #   NETWORK_FEE - @transaction['tx']['fees']
  # end

  def create_unique_token
    loop do
      self.token = SecureRandom.hex(10) # or whatever you chose like UUID tools
      break unless self.class.exists?(token: token)
    end
  end

  def available
    unless AVAILABLE
      errors.add(:destination, 'Saque temporariamente indisponível. Estamos em manutenção. Em breve estará de volta :)')
      false
    end
  end
end

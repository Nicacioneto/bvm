# frozen_string_literal: true

class IndirectCommission < Commission
  after_create :generate_indirect_commission

  def generate_indirect_commission
    commission_rate = 0.01
    self.value = commission_rate * investiment.value
    user.account.credit_available(value)
    update(paid_at: DateTime.now, status: :paid, commission_rate: commission_rate)
    NotifierReceivedCommissionsMailer.notify_received_commission(self.user, self.investiment, self).deliver_now
  end
end

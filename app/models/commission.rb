# frozen_string_literal: true

class Commission < ApplicationRecord
  extend Enumerize

  enumerize :status, in: %i[pending paid reversed], default: :pending
  belongs_to :user
  belongs_to :investiment
end

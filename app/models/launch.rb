class Launch < ApplicationRecord
    extend Enumerize

    belongs_to :account
    enumerize :type_of_balance, in: [:available_balance]
    validates :value, :presence => true
    after_create :send_to_available

private
    
    def send_to_available
        if self.value > 0
            self.account.credit_available(value)
        elsif self.value < 0    
            self.account.debit_available(value)
        end
    end
end
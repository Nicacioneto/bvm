# frozen_string_literal: true

class Investiment < ApplicationRecord
  extend Enumerize

  has_paper_trail
  enumerize :status, in: %i[pending activated disabled canceled ], default: :pending
  belongs_to :user
  belongs_to :deposit, optional: true
  has_many :remunerations
  has_many :commissions

  def credit_accumulated(value)
    final_value = accumulated_value + value.abs
    update_attribute(:accumulated_value, final_value.round(2))
  end

  def create_with_balance
    if value >= 250
      if user.account.has_enough_available_funds?(value) && 
        user.account.debit_available(value)
        self.accumulated_value = self.value
        save
      else
        errors.add(:amount, I18n.t('validations.withdrawals.insufficient_funds'))
        false
      end
    else
      errors.add(:amount, I18n.t('validations.withdrawals.minimal_investiment'))
      false
    end 
  end


  def create_with_balance_by_admin
    if user.account.has_enough_available_funds?(value) && 
      user.account.debit_available(value)
      self.accumulated_value = self.value
      save
    else
      errors.add(:amount, I18n.t('validations.withdrawals.insufficient_funds'))
      false
    end
  end

  def activate
    if status.pending?
      set_expiration_date
      generate_remunerations
      update(status: :activated, activated_at: DateTime.now)     
      generate_direct_commissions
      generate_indirect_commissions
      NotifierActivatedInvestimentsMailer.notify_activated_investiment(self.user, self).deliver_now
    else
      false
    end
  end


  def disable
    if status.activated? && self.remunerations.where(status: :pending).empty?
      user.account.credit_available(accumulated_value)
      update(status: :disabled, disabled_at: DateTime.now)
      direct_indicator = User.find_by_id(user.user_id)
      direct_indicator&.update_consultor_level
    else
      false
    end
  end
  
  def edit_values(value, accumulated_value)
    if value.to_f != self.value || accumulated_value.to_f != self.accumulated_value
      self.update(value: value, accumulated_value: accumulated_value)
      remuneration_value = get_remuneration_value
      self.remunerations.update_all(value: remuneration_value)
    end
  end
  
  private

  def set_expiration_date
    update_attribute(:expiration_date, Date.today + 30.day)
  end

  def generate_remunerations
    initial_date = Date.today + 1.day
    expiration_date = self.expiration_date
    remuneration_value = get_remuneration_value
    iterator = 1

    (initial_date..expiration_date).each do |_date|
      Remuneration.create(number: iterator, value: remuneration_value, remuneration_date: _date, investiment: self)
      iterator += 1
    end
  end

  def generate_direct_commissions
    direct_indicator = User.find_by_id(user.user_id)
    direct_indicator&.update_consultor_level
    if direct_indicator.present?
      DirectCommission.create(user_id: direct_indicator.id, investiment_id: id)
    end
  end

  def generate_indirect_commissions
    direct_indicator = User.find_by_id(user.user_id)
    indirect_indicator = User.find_by_id(direct_indicator&.user_id)
    if direct_indicator.present? && indirect_indicator.present? && indirect_indicator.level.franchise?
      IndirectCommission.create(user_id: indirect_indicator.id, investiment_id: id)
    end
  end

  def get_remuneration_value
    (((remuneration_rate / 100) / 30.0) * value).round(2)
  end
end

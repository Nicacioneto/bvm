class BrlDeposit < Deposit
  FEE = 0

  before_create :default_values, :bank_account_sys_is_valid?
  after_create :send_email_to_collect_voucher
  before_update :check_deposit_receipt

  belongs_to :account

  belongs_to :bank_account_client
  belongs_to :bank_account_sys_admin
  has_one :user, through: :brl_wallet
  has_one :bank, through: :bank_account_client
  has_one :bank, through: :bank_account_sys_admin

  validates :deposit_form, :presence => {:message => I18n.t("validations.deposit_deposit_form")}
  validates :bank_account_client, :presence => {:message => I18n.t("validations.deposit_bank_account_client")}
  validates :bank_account_sys_admin, :presence => {:message => I18n.t("validations.deposit_bank_account_sys_admin")}
  validates :amount, :presence => {:message => I18n.t("validations.deposit_amount")}
  validates :amount, numericality: {greater_than_or_equal_to: 100, :message => I18n.t("validations.deposits_amount_numericality")}

  mount_uploader :voucher, DepositUploader

  def cancel
    self.update(status: :canceled, cancel_receipt: Time.zone.now)
  end

  def update_attempt(params)
    self.update(params)
  end

  def approve
    self.update(status: :confirmed, confirm_receipt: Time.zone.now)
    credit
  end

  def disapprove
    self.update_attribute(:status, :canceled)
    self.update_attribute(:cancel_receipt, Time.zone.now)
  end

  def send_email_to_collect_voucher
    # Message.create(message_type: :deposit, title: I18n.t("default_messages.deposit_receipt.title"), message: I18n.t("default_messages.deposit_receipt.message"), user: self.user)
    # if Rails.env.production? || Rails.env.staging?
    #   DepositNotifierMailer.notifier_to_collect_voucher(self).deliver_now
    # end
  end

  def export_date
    self.confirm_receipt.to_s
  end

  def status_translated
    I18n.t("status." + self.status)
  end

  def amount_in_coin
    "R$ " + final_amount.to_brl.to_s
  end

  def type_translated
    "Depósito BRL"
  end

  def users_name
    self.user.name
  end

  private
  
  def check_deposit_receipt
    if self.status == "requested" and self.voucher.present?
      self.status = "sent"
    end
  end

  def default_values
    self.final_amount = self.amount - (self.amount * FEE)
    self.status = "requested"
  end

  def credit
    final_amount = self.amount - (self.amount * FEE)
    if self.brl_wallet.credit(final_amount)
      send_email_when_approve_deposit
    end
  end

  def send_email_when_approve_deposit
    Message.create(message_type: :deposit, title: I18n.t("default_messages.confirm_deposit.title"), message: I18n.t("default_messages.confirm_deposit.message", amount: self.final_amount.to_brl), user: self.user)
    if Rails.env.production? || Rails.env.staging?
      ApproveDepositNotifierMailer.notifier_approve_deposit(self).deliver_now
    end
  end

  def bank_account_sys_is_valid?
    bank_sys_deposit = BankAccountSysAdmin.find_by_id(self.bank_account_sys_admin_id)
    unless bank_sys_deposit.present? || bank_sys_deposit.status.activated?
      raise ActiveRecord::Rollback and return false
    end
  end
end

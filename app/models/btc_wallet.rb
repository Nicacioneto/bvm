# frozen_string_literal: true

class BtcWallet < ApplicationRecord
  belongs_to :user
  has_many :deposits
  before_create :set_address unless Rails.env.development?

  private

  def set_address
    wallet = blockcypher.create_forwarding_address(Rails.application.credentials[Rails.env.to_sym][:wallets][:cold_wallet_address], callback_url: Rails.application.credentials[Rails.env.to_sym][:host_path] + '/548d82xh4gg5xt6cy8chvxnm6nmxvk6g')
    self.address = wallet['input_address']
    self.details = wallet.to_s.gsub('=>', ':')
    if address.nil? || details.nil?
      errors.add(:address, :blank, message: 'cannot be nil')
      errors.add(:details, :blank, message: 'cannot be nil')
      raise ActiveRecord::Rollback
    end
  end
end

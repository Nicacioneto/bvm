class BankAccountClient < ApplicationRecord
	extend Enumerize
	
	belongs_to :account
	belongs_to :bank
	has_many :deposits
	# has_many :withdraws

	validates :bank , :presence => {message: I18n.t("validations.cant_be_blank", attribute_name: I18n.t("bank_account_clients.bank"))}
	validates :agency , :presence  => {message: I18n.t("validations.cant_be_blank", attribute_name: I18n.t("bank_account_clients.agency"))}
	validates :account_number , :presence => {message: I18n.t("validations.cant_be_blank", attribute_name: I18n.t("bank_account_clients.account_number"))}
  	before_create :initialize_params

	validates :agency, :length => {in: 2..10,
									too_short: I18n.t("validations.bank_account_client.agency_minimum"),
									too_long: I18n.t("validations.bank_account_client.agency_maximum")}

	validates :account_number, numericality: {greater_than_or_equal_to: 0, :message => I18n.t("validations.bank_account_client.agency_numericality")}
	validates :account_number, :length => {in: 2..15,
									too_short: I18n.t("validations.bank_account_client.account_number_minimum"),
									too_long: I18n.t("validations.bank_account_client.account_number_maximum")}

	# validates :digit, numericality: {greater_than_or_equal_to: 0, :message => I18n.t("validations.bank_account_client.digit_numericality")}
	validates :digit, :length => {is: 1, message:  I18n.t("validations.bank_account_client.digit_length")}

	validates :operation, :length => {maximum: 3, message:  I18n.t("validations.bank_account_client.operation_length")}

	enumerize :status, in: [:activated, :deactivated], default: :activated
	enumerize :bank_account_type, in: [:checking, :savings], default: :checking

	def deactivate
		self.update_attribute(:status, :deactivated)
	end

  def bank_id=(value)
    if value.is_a?(Integer)
      write_attribute(:bank_id, value)
    elsif value.is_a?(String)
      write_attribute(:bank_id, Bank.find_by_cod(value.to_i).id)
    end
	end
	
	def self.map_banks_by_account(account)
		BankAccountClient.where(account: account, status: :activated).includes(:bank)
      .map { |bank_account| ["#{bank_account.bank.name} - Ag: #{bank_account.agency} - Conta: #{bank_account.account_number}", bank_account.id] }
	end

  private
  def initialize_params
    self.owner_name = account.user.name
    if account.user.country == "BR"
    	self.doc_number = account.user.cpf
	  else
	  	self.doc_number = account.user.cpf
	  end
  end
end
class Address < ApplicationRecord
  after_create :verify_user

  belongs_to :user
  validates :address1, presence: true, length: {minimum: 5}
  validates :city, presence: true
  validates :zipcode, presence: true
  validates :state, presence: true
  validates :country, presence: true

  def verify_user
    self.user.update_attribute(:status, :verified)
  end
end

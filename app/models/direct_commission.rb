# frozen_string_literal: true

class DirectCommission < Commission
  after_create :generate_direct_commission

  def generate_direct_commission
    if investiment.user.investiments.count == 1
      commission_rate = 0.05
      self.value = commission_rate * investiment.value
    elsif user.level.ib_one?
      commission_rate = 0.01
      self.value = commission_rate * investiment.value
    elsif user.level.ib_two?
      commission_rate = 0.02
      self.value = commission_rate * investiment.value
    elsif user.level.ib_three? || user.level.franchise?
      commission_rate = 0.03
      self.value = commission_rate * investiment.value
    end
    user.account.credit_available(value)
    update(paid_at: DateTime.now, status: :paid, commission_rate: commission_rate)
    NotifierReceivedCommissionsMailer.notify_received_commission(self.user, self.investiment, self).deliver_now
  end
end

# frozen_string_literal: true

class Remuneration < ApplicationRecord
  extend Enumerize

  belongs_to :investiment
  enumerize :status, in: %i[pending paid reversed], default: :pending

  def pay
    if status.pending?
      investiment.credit_accumulated(value)
      update(status: :paid, paid_at: DateTime.now)
    else
      errors.add(:status, 'This remuneration is already paid!')
      false
    end
  end
end

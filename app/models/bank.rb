class Bank < ApplicationRecord
    has_many :bank_account_clients
    has_many :bank_account_sys_admins
    validates :cod , :presence => {message: I18n.t("validations.cant_be_blank", attribute_name: I18n.t("banks.bank_code"))}
    validates :name , :presence => {message: I18n.t("validations.cant_be_blank", attribute_name: I18n.t("banks.bank_name"))}
    
    extend Enumerize
      enumerize :status, in: [:activated, :deactivated], default: :activated
  
    def to_s
      self.name
    end
  
  end
  
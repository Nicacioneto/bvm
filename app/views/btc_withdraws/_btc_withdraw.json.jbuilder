json.extract! btc_withdraw, :id, :transaction_hash, :status, :fee, :amount, :confirmations, :destination, :absorved_rate, :token, :user_id, :created_at, :updated_at
json.url btc_withdraw_url(btc_withdraw, format: :json)

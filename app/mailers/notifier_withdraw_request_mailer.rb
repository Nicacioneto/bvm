class NotifierWithdrawRequestMailer < ApplicationMailer
  def request_confirmation(user, btc_withdraw, link_to_confirm)
		@user = user
		@btc_withdraw_amount = btc_withdraw.amount.to_bitcoin
		@btc_withdraw = btc_withdraw
		@confirm_link = link_to_confirm
		if btc_withdraw.fee.nil?
			@btc_withdraw_fee = 0
		else
			@btc_withdraw_fee = btc_withdraw.fee.to_bitcoin
		end
		
		mail(
				to: @user.email,
				subject: I18n.t("mailers.btc_withdraw.subject") + " - BTC " + @btc_withdraw_amount + " - " + DateTime.now.strftime('%d %b %Y %H:%M:%S'))
	end
end

class NotifierExpiredInvestimentsMailer < ApplicationMailer
  def notify_expired_investiment(user, investiment)
    @user = user
    @investiment = investiment

    mail(
      to: @user.email,
      subject: I18n.t("mailers.expired_investiments.subject"))
  end

  def notify_consultor_for_expired_investiment(consultor, user, investiment)
    @consultor = consultor
    @user = user
    @investiment = investiment

    mail(
      to: @consultor.email,
      subject: I18n.t("mailers.expired_investiments.subject"))
  end
end

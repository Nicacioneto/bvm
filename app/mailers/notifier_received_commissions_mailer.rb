class NotifierReceivedCommissionsMailer < ApplicationMailer
  def notify_received_commission(user, investiment, commission)
    @user = user
    @investiment = investiment
    @commission = commission
    mail(
      to: @user.email,
      subject: I18n.t("mailers.received_commissions.subject"))
  end
end

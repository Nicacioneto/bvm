class DepositsNotifierMailer < ApplicationMailer
  def send_confirmation_deposit(user, coin_deposit)
    @user = user
    @coin_deposit_amount = coin_deposit.amount.to_bitcoin
    @coin_deposit = coin_deposit
    if coin_deposit.fee.nil?
      @coin_deposit_fee = 0
    else
      @coin_deposit_fee = coin_deposit.fee.to_bitcoin
    end

    mail(
      to: @user.email,
      subject: I18n.t("mailers.coin_deposit.subject"))
  end

end

class ApplicationMailer < ActionMailer::Base
  default from: 'contato@app.fortknofc.com'
  layout 'mailer'
end

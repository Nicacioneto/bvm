class NotifierActivatedInvestimentsMailer < ApplicationMailer
  def notify_activated_investiment(user, investiment)
    @user = user
    @investiment = investiment

    mail(
      to: @user.email,
      subject: I18n.t("mailers.activated_investiments.subject"))
  end
end

class ContactsMailer < ApplicationMailer
  def general_message(contact)
    @contact = contact
    mail(to: "support@bvm.com", subject: "Você possui uma nova mensagem")
  end
end

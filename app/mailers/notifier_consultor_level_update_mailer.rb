class NotifierConsultorLevelUpdateMailer < ApplicationMailer
  def notify_level_update(user)
    @user = user
    mail(
      to: @user.email,
      subject: I18n.t("mailers.level_update.subject"))
  end
end

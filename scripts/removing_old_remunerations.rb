
  investiments_with_problem = []
  Investiment.all.each do |f|
    if f.remunerations.size > 30
      investiments_with_problem << f
    end
  end    

  investiments_with_problem.each do |f|
    f.remunerations.where("remuneration_date < ?", f.renewal_at).destroy_all
    f.remunerations.where("number > ?", 30).destroy_all
    if f.renewal_at.present?
      f.update(created_at: f.renewal_at)
    else
      f.update(created_at: f.remunerations.first.remuneration_date - 1.day)
    end  
  end

  Remuneration.where(status: :paid).each do |f|
    f.update(paid_at: f.remuneration_date + 1.day)
  end



# investiments_with_problem = []
# Investiment.all.each do |f|
#   if f.created_at + 31.day < f.expiration_date
#     investiments_with_problem << f
#     f.update(created_at: f.expiration_date - 30.day)
#   end
# end

user = User.find(45)
investiment = Investiment.new(user: user, remuneration_rate: 10, value: user.account.available_balance)
investiment.create_with_balance
investiment.activate
def paid_remunerations(investiment)
  investiment.remunerations.where(status: :paid).sum(:value)
end

Investiment.where('accumulated_value < value').each do |f|
  f.update(accumulated_value: f.value + paid_remunerations(f))
end

def check_accumulated_balances
  users_with_problems = []
  User.all.each do |user|
    old_accumulated_balance = user.account.accumulated_balance + user.investiments.where(status: :activated).sum(:value)
    if old_accumulated_balance != user.investiments.where(status: :activated).sum(:accumulated_value) 
      users_with_problems << [user.email, old_accumulated_balance,  user.investiments.where(status: :activated).sum(:accumulated_value)]
    end
  end 
  return users_with_problems
end
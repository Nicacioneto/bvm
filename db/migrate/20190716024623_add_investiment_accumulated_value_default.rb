class AddInvestimentAccumulatedValueDefault < ActiveRecord::Migration[5.2]
  def change
    change_column :investiments, :accumulated_value, :decimal, :default => 0
  end
end

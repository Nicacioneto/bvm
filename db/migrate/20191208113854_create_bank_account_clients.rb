# frozen_string_literal: true

class CreateBankAccountClients < ActiveRecord::Migration[5.2]
  def change
    create_table 'bank_account_clients' do |t|
      t.string 'owner_name', null: false
      t.string 'cpf'
      t.string 'agency', null: false
      t.string 'account_number', null: false
      t.string 'digit'
      t.integer 'operation'
      t.boolean 'joint'
      t.bigint 'account_id'
      t.bigint 'bank_id'
      t.string 'status'
      t.string 'bank_account_type'
      t.string 'doc_number'
      t.datetime 'created_at', null: false
      t.datetime 'updated_at', null: false
    end
  end
end

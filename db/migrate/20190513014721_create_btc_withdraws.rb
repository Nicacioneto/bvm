# frozen_string_literal: true

class CreateBtcWithdraws < ActiveRecord::Migration[5.2]
  def change
    create_table :btc_withdraws do |t|
      t.string :transaction_hash
      t.string :status
      t.bigint :fee
      t.bigint :amount
      t.integer :confirmations
      t.string :destination
      t.bigint :absorved_rate
      t.string :token
      t.decimal :value
      t.decimal :btc_cotation
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end

# frozen_string_literal: true

class AddDepositFieldsToDeposits < ActiveRecord::Migration[5.2]
  def change
    add_column :deposits, :voucher, :string
    add_column :deposits, :deposit_form, :string
    add_column :deposits, :send_receipt, :datetime
    add_column :deposits, :confirm_receipt, :datetime
    add_column :deposits, :cancel_receipt, :datetime
    add_column :deposits, :bank_account_client_id, :bigint
    add_column :deposits, :bank_account_sys_admin_id, :bigint
    add_column :deposits, :account_id, :bigint
    add_column :deposits, :type, :string
  end
end

# frozen_string_literal: true

class CreateBankAccountSysAdmins < ActiveRecord::Migration[5.2]
  def change
    create_table 'bank_account_sys_admins' do |t|
      t.integer 'agency'
      t.integer 'account_number'
      t.integer 'digit'
      t.integer 'operation'
      t.boolean 'joint'
      t.datetime 'created_at', null: false
      t.datetime 'updated_at', null: false
      t.bigint 'bank_id'
      t.string 'status'
      t.string 'owner_name'
      t.string 'cnpj'
    end
  end
end

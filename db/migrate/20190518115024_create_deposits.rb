# frozen_string_literal: true

class CreateDeposits < ActiveRecord::Migration[5.2]
  def change
    create_table :deposits do |t|
      t.bigint :amount
      t.bigint :fee
      t.bigint :final_amount
      t.string :status
      t.string :transaction_hash
      t.integer :confirmations
      t.string :sender
      t.string :prev_hash
      t.decimal :value
      t.decimal :btc_cotation
      t.references :btc_wallet, foreign_key: true

      t.timestamps
    end
  end
end

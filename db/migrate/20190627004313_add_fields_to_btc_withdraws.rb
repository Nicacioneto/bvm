class AddFieldsToBtcWithdraws < ActiveRecord::Migration[5.2]
  def change
    add_column :btc_withdraws, :final_amount, :bigint
    add_column :btc_withdraws, :penalty_rate, :decimal
    add_column :btc_withdraws, :penalty_value, :decimal
    add_column :btc_withdraws, :penalty_amount, :decimal
  end
end

class AddAccumulatedValueToInvestiments < ActiveRecord::Migration[5.2]
  def change
    add_column :investiments, :accumulated_value, :decimal
  end
end

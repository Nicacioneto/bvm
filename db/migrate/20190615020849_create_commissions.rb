# frozen_string_literal: true

class CreateCommissions < ActiveRecord::Migration[5.2]
  def change
    create_table :commissions do |t|
      t.references :user, foreign_key: true
      t.references :investiment, foreign_key: true
      t.decimal :value
      t.decimal :commission_rate
      t.datetime :paid_at
      t.string :type
      t.string :status

      t.timestamps
    end
  end
end

class CreatePackages < ActiveRecord::Migration[5.2]
  def change
    create_table :packages do |t|
      t.string :name
      t.decimal :remuneration_rate

      t.timestamps
    end
  end
end

class CreateHotWallets < ActiveRecord::Migration[5.2]
  def change
    create_table :hot_wallets do |t|
      t.string :address
      t.string :api_key

      t.timestamps
    end
  end
end

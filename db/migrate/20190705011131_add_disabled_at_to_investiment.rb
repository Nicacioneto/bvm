class AddDisabledAtToInvestiment < ActiveRecord::Migration[5.2]
  def change
    add_column :investiments, :disabled_at, :datetime
  end
end

class CreateLaunches < ActiveRecord::Migration[5.2]
  def change
    create_table :launches do |t|
      t.integer :value
      t.string :type_of_balance
      t.string :observation
      t.references :account, foreign_key: true

      t.timestamps
    end
  end
end

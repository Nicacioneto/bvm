class ChangeLaunchValueToBeDecimal < ActiveRecord::Migration[5.2]
  def change
    change_column :launches, :value, :decimal
  end
end

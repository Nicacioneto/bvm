# frozen_string_literal: true

class CreateRemunerations < ActiveRecord::Migration[5.2]
  def change
    create_table :remunerations do |t|
      t.integer :number
      t.decimal :value
      t.string :status
      t.date :remuneration_date
      t.datetime :paid_at
      t.references :investiment, foreign_key: true

      t.timestamps
    end
  end
end

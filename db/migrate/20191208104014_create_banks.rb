# frozen_string_literal: true

class CreateBanks < ActiveRecord::Migration[5.2]
  def change
    create_table 'banks' do |t|
      t.integer 'cod', null: false
      t.string 'name', null: false
      t.string 'status'
      t.datetime 'created_at', null: false
      t.datetime 'updated_at', null: false
    end
  end
end

# frozen_string_literal: true

class CreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      t.decimal :available_balance, default: 0
      t.decimal :accumulated_balance, default: 0
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end

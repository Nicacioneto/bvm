class CreateBtcWallets < ActiveRecord::Migration[5.2]
  def change
    create_table :btc_wallets do |t|
      t.string :address
      t.bigint :balance, default: 0, null: false
      t.string :details
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end

# frozen_string_literal: true

class CreateInvestiments < ActiveRecord::Migration[5.2]
  def change
    create_table :investiments do |t|
      t.decimal :value
      t.date :expiration_date
      t.datetime :renewal_at
      t.datetime :activated_at
      t.decimal :remuneration_rate
      t.references :user, foreign_key: true
      t.string :status, default: :pending
      t.references :deposit, foreign_key: true

      t.timestamps
    end
  end
end

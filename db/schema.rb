# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_12_10_232623) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.decimal "available_balance", default: "0.0"
    t.decimal "accumulated_balance", default: "0.0"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_accounts_on_user_id"
  end

  create_table "addresses", force: :cascade do |t|
    t.string "address1"
    t.string "address2"
    t.string "city"
    t.string "zipcode"
    t.string "state"
    t.string "country"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_addresses_on_user_id"
  end

  create_table "admins", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "name"
    t.string "type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  end

  create_table "bank_account_clients", force: :cascade do |t|
    t.string "owner_name", null: false
    t.string "cpf"
    t.string "agency", null: false
    t.string "account_number", null: false
    t.string "digit"
    t.integer "operation"
    t.boolean "joint"
    t.bigint "account_id"
    t.bigint "bank_id"
    t.string "status"
    t.string "bank_account_type"
    t.string "doc_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "bank_account_sys_admins", force: :cascade do |t|
    t.integer "agency"
    t.integer "account_number"
    t.integer "digit"
    t.integer "operation"
    t.boolean "joint"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "bank_id"
    t.string "status"
    t.string "owner_name"
    t.string "cnpj"
  end

  create_table "banks", force: :cascade do |t|
    t.integer "cod", null: false
    t.string "name", null: false
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "btc_wallets", force: :cascade do |t|
    t.string "address"
    t.bigint "balance", default: 0, null: false
    t.string "details"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_btc_wallets_on_user_id"
  end

  create_table "btc_withdraws", force: :cascade do |t|
    t.string "transaction_hash"
    t.string "status"
    t.bigint "fee"
    t.bigint "amount"
    t.integer "confirmations"
    t.string "destination"
    t.bigint "absorved_rate"
    t.string "token"
    t.decimal "value"
    t.decimal "btc_cotation"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "final_amount"
    t.decimal "penalty_rate"
    t.decimal "penalty_value"
    t.decimal "penalty_amount"
    t.index ["user_id"], name: "index_btc_withdraws_on_user_id"
  end

  create_table "commissions", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "investiment_id"
    t.decimal "value"
    t.decimal "commission_rate"
    t.datetime "paid_at"
    t.string "type"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["investiment_id"], name: "index_commissions_on_investiment_id"
    t.index ["user_id"], name: "index_commissions_on_user_id"
  end

  create_table "contacts", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "deposits", force: :cascade do |t|
    t.bigint "amount"
    t.bigint "fee"
    t.bigint "final_amount"
    t.string "status"
    t.string "transaction_hash"
    t.integer "confirmations"
    t.string "sender"
    t.string "prev_hash"
    t.decimal "value"
    t.decimal "btc_cotation"
    t.bigint "btc_wallet_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "voucher"
    t.string "deposit_form"
    t.datetime "send_receipt"
    t.datetime "confirm_receipt"
    t.datetime "cancel_receipt"
    t.bigint "bank_account_client_id"
    t.bigint "bank_account_sys_admin_id"
    t.bigint "account_id"
    t.string "type"
    t.index ["btc_wallet_id"], name: "index_deposits_on_btc_wallet_id"
  end

  create_table "hot_wallets", force: :cascade do |t|
    t.string "address"
    t.string "api_key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "investiments", force: :cascade do |t|
    t.decimal "value"
    t.date "expiration_date"
    t.datetime "renewal_at"
    t.datetime "activated_at"
    t.decimal "remuneration_rate"
    t.bigint "user_id"
    t.string "status", default: "pending"
    t.bigint "deposit_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "disabled_at"
    t.decimal "accumulated_value", default: "0.0"
    t.index ["deposit_id"], name: "index_investiments_on_deposit_id"
    t.index ["user_id"], name: "index_investiments_on_user_id"
  end

  create_table "launches", force: :cascade do |t|
    t.decimal "value"
    t.string "type_of_balance"
    t.string "observation"
    t.bigint "account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_launches_on_account_id"
  end

  create_table "packages", force: :cascade do |t|
    t.string "name"
    t.decimal "remuneration_rate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "remunerations", force: :cascade do |t|
    t.integer "number"
    t.decimal "value"
    t.string "status"
    t.date "remuneration_date"
    t.datetime "paid_at"
    t.bigint "investiment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["investiment_id"], name: "index_remunerations_on_investiment_id"
  end

  create_table "reports", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "name"
    t.string "cpf"
    t.string "phone"
    t.string "phone_code"
    t.string "country"
    t.string "status", default: "pending"
    t.string "level", default: "basic"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.string "avatar"
    t.string "username"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["user_id"], name: "index_users_on_user_id"
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.bigint "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.json "object"
    t.datetime "created_at"
    t.text "object_changes"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  add_foreign_key "accounts", "users"
  add_foreign_key "addresses", "users"
  add_foreign_key "btc_wallets", "users"
  add_foreign_key "btc_withdraws", "users"
  add_foreign_key "commissions", "investiments"
  add_foreign_key "commissions", "users"
  add_foreign_key "deposits", "btc_wallets"
  add_foreign_key "investiments", "deposits"
  add_foreign_key "investiments", "users"
  add_foreign_key "launches", "accounts"
  add_foreign_key "remunerations", "investiments"
end

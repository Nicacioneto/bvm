# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

puts 'Destruindo Usuarios'
User.destroy_all

puts 'Criando Usuarios'
User.create(email: 'jorge@client.com', username: 'jorge', cpf: CPF.generate, name: 'Jorge Aragão', phone_code: '55', phone: '61982011835', country: 'Brazil', password: '123123', password_confirmation: '123123', confirmed_at: DateTime.now, level: :franchise)

User.create(email: 'amy@client.com', username: 'amyadams', cpf: CPF.generate, name: 'Amy Adams', phone_code: '55', phone: '61982011835', country: 'Brazil', password: '123123', password_confirmation: '123123', confirmed_at: DateTime.now, level: :ib_one, user_id: User.first.id)

User.create(email: 'gal@client.com', username: 'galgadot', cpf: CPF.generate, name: 'Gal Gadot', phone_code: '55', phone: '61982011835', country: 'Brazil', password: '123123', password_confirmation: '123123', confirmed_at: DateTime.now, user_id: User.second.id)

User.create(email: 'daisy@client.com',  username: 'daisyridley',  cpf: CPF.generate, name: 'Daisy Ridley', phone_code: '55', phone: '61982011835', country: 'Brazil', password: '123123', password_confirmation: '123123', confirmed_at: DateTime.now, user_id: User.second.id)

10.times do
  User.create(email: Faker::Internet.email, username: "#{Faker::Internet.user_name(%w[_])}#{Faker::Internet.user_name(%w[_])}", cpf: CPF.generate, name: Faker::Name.name, phone_code: '55', phone: '61982011835', country: 'Brazil', password: '123123', password_confirmation: '123123', confirmed_at: DateTime.now, user_id: User.first.id)
end

puts 'Destruindo Admins'
Admin.destroy_all

puts 'Criando Admins'

Admin.create(email: 'admin@admin.com', password: '123123', password_confirmation: '123123', confirmed_at: DateTime.now)

puts 'Destruindo Endereços'
Address.destroy_all

puts 'Criando Endereço'
Address.create(address1: Faker::Address.street_address, address2: Faker::Address.secondary_address, city: Faker::Address.city, zipcode: Faker::Address.zip_code, state: Faker::Address.state, country: Faker::Address.country, user: User.first)

Address.create(address1: Faker::Address.street_address, address2: Faker::Address.secondary_address, city: Faker::Address.city, zipcode: Faker::Address.zip_code, state: Faker::Address.state, country: Faker::Address.country, user: User.second)

Address.create(address1: Faker::Address.street_address, address2: Faker::Address.secondary_address, city: Faker::Address.city, zipcode: Faker::Address.zip_code, state: Faker::Address.state, country: Faker::Address.country, user: User.second)

Address.create(address1: Faker::Address.street_address, address2: Faker::Address.secondary_address, city: Faker::Address.city, zipcode: Faker::Address.zip_code, state: Faker::Address.state, country: Faker::Address.country, user: User.second)

# puts 'Destruindo HotWallet'
# HotWallet.destroy_all

# puts 'Criando HotWallet'
# HotWallet.create(address: Rails.application.credentials[Rails.env.to_sym][:wallets][:hot_wallet_address], api_key: Rails.application.credentials[Rails.env.to_sym][:wallets][:api_key])

# puts 'Destruindo BtcWallets'
# BtcWallet.destroy_all

# puts 'Criando BtcWallets'
# BtcWallet.create(user: User.first, address: 'CEXvvuuCFfk1sHkDTGevRPSv9FgaBjCePp',
#                  details: '{"id":"a0c39eb4-4f4a-41c7-aacd-e3c39a8b3fb3", "token":"f559581fbebb412b9379cd5752bcae49", "destination":"C7BRjFbDLuTqj5cnbU1sBEc9ZHg6M8K7ms", "input_address":"CEXvvuuCFfk1sHkDTGevRPSv9FgaBjCePp", "callback_url":"http://localhost:3000/548d82xh4gg5xt6cy8chvxnm6nmxvk6g"}')
# BtcWallet.create(user: User.second, address: 'CA8eZQxMR4k8WDsNHo7zDnrovDDUFmpSgB',
#                  details: '{"id":"4ac70ea1-44ce-481a-bec9-4ac64df854d2", "token":"f559581fbebb412b9379cd5752bcae49", "destination":"C7BRjFbDLuTqj5cnbU1sBEc9ZHg6M8K7ms", "input_address":"CA8eZQxMR4k8WDsNHo7zDnrovDDUFmpSgB", "callback_url":"https://4b9b2670.ngrok.io/548d82xh4gg5xt6cy8chvxnm6nmxvk6g"}')
# BtcWallet.create(user: User.third, address: 'C5WksneMdTd7JQMmZMdyuYeADmdbE75N7q',
#                  details: '{"id":"2a844561-78d8-4922-8d52-69fd393dc058", "token":"f559581fbebb412b9379cd5752bcae49", "destination":"C7BRjFbDLuTqj5cnbU1sBEc9ZHg6M8K7ms", "input_address":"C5WksneMdTd7JQMmZMdyuYeADmdbE75N7q", "callback_url":"https://4b9b2670.ngrok.io/548d82xh4gg5xt6cy8chvxnm6nmxvk6g"}')
# BtcWallet.create(user: User.fourth, address: 'C4VMPgCqe973gcVsy1nHN4sTRbuter19eA',
#                  details: '{"id":"129139e4-91d3-4203-bed2-935bec2c864c", "token":"f559581fbebb412b9379cd5752bcae49", "destination":"C7BRjFbDLuTqj5cnbU1sBEc9ZHg6M8K7ms", "input_address":"C4VMPgCqe973gcVsy1nHN4sTRbuter19eA", "callback_url":"https://4b9b2670.ngrok.io/548d82xh4gg5xt6cy8chvxnm6nmxvk6g"}')

# puts 'Destruindo Depósitos'
# Deposit.destroy_all

# puts 'Criando Depósitos'
# Deposit.create(amount: 1_000_000,
#                fee: 40_000,
#                final_amount: nil,
#                status: 'confirmed',
#                transaction_hash: 'cae962126f79d032a7432194e5b128ca7886009bddf4bc7df2254f98ff8eaca9',
#                confirmations: 0,
#                sender: 'CATHDLH66eXbdFVVQwAVP2TeJY2pEsGrNt',
#                prev_hash: '57acc6ec2d9967ea8cd4c901c17a840f4cc176265736f7aeec30ff7a65b0e756',
#                value: 1000,
#                btc_cotation: 7989.73,
#                btc_wallet_id: 2)

# puts 'Criando pacotes'               
# Package.create(remuneration_rate: 9.99)

# puts 'Criando Investimento'
# Investiment.new(user: User.second, remuneration_rate: Package.first.remuneration_rate, value: 1000).create_with_balance

# puts 'Ativando Investimento'
# Investiment.last.activate

# puts 'Criando Depósitos'
# Deposit.create(amount: 1_000_000,
#                fee: 40_000,
#                final_amount: nil,
#                status: 'confirmed',
#                transaction_hash: 'de27abc0ff500eeb31290ed6b1905048845aab5d43359a7e81c0473777400d15',
#                confirmations: 0,
#                sender: 'CATHDLH66eXbdFVVQwAVP2TeJY2pEsGrNt',
#                prev_hash: '57acc6ec2d9967ea8cd4c901c17a840f4cc176265736f7aeec30ff7a65b0e756',
#                value: 1000,
#                btc_cotation: 7989.73,
#                btc_wallet_id: 3)

# puts 'Criando Investimento'
# Investiment.new(user: User.third, remuneration_rate: Package.first.remuneration_rate, value: 1000).create_with_balance

# puts 'Ativando Investimento'
# Investiment.last.activate

# puts 'Criando Depósitos'
# Deposit.create(amount: 1_000_000,
#                fee: 40_000,
#                final_amount: nil,
#                status: 'confirmed',
#                transaction_hash: 'ade27abc0ff500eeb31290ed6b1905048845aab5d43359a7e81c0473777400d15',
#                confirmations: 0,
#                sender: 'CATHDLH66eXbdFVVQwAVP2TeJY2pEsGrNt',
#                prev_hash: '57acc6ec2d9967ea8cd4c901c17a840f4cc176265736f7aeec30ff7a65b0e756',
#                value: 1000,
#                btc_cotation: 7989.73,
#                btc_wallet_id: 4)

# puts 'Criando Investimento'
# Investiment.new(user: User.fourth, remuneration_rate: Package.first.remuneration_rate, value: 1000).create_with_balance

# puts 'Ativando Investimento'
# Investiment.last.activate

# puts 'Criando Depósitos'
# Deposit.create(amount: 1_000_000,
#                fee: 40_000,
#                final_amount: nil,
#                status: 'confirmed',
#                transaction_hash: 'xade27abc0ff500eeb31290ed6b1905048845aab5d43359a7e81c0473777400d15',
#                confirmations: 0,
#                sender: 'CATHDLH66eXbdFVVQwAVP2TeJY2pEsGrNt',
#                prev_hash: '57acc6ec2d9967ea8cd4c901c17a840f4cc176265736f7aeec30ff7a65b0e756',
#                value: 1000,
#                btc_cotation: 7989.73,
#                btc_wallet_id: 2)

# puts 'Criando Investimento'
# Investiment.new(user: User.second, remuneration_rate: Package.first.remuneration_rate, value: 1000).create_with_balance

# puts 'Ativando Investimento'
# Investiment.last.activate

# puts 'Criando Depósitos'
# Deposit.create(amount: 1_000_000,
#                fee: 40_000,
#                final_amount: nil,
#                status: 'confirmed',
#                transaction_hash: 'xyade27abc0ff500eeb31290ed6b1905048845aab5d43359a7e81c0473777400d15',
#                confirmations: 0,
#                sender: 'CATHDLH66eXbdFVVQwAVP2TeJY2pEsGrNt',
#                prev_hash: '57acc6ec2d9967ea8cd4c901c17a840f4cc176265736f7aeec30ff7a65b0e756',
#                value: 1000,
#                btc_cotation: 7989.73,
#                btc_wallet_id: 3)

# puts 'Criando Investimento'
# Investiment.new(user: User.third, remuneration_rate: Package.first.remuneration_rate, value: 1000).create_with_balance


# puts 'Ativando Investimento'
# Investiment.last.activate

# puts 'Criando Depósitos'
# Deposit.create(amount: 1_000_000,
#                fee: 40_000,
#                final_amount: nil,
#                status: 'confirmed',
#                transaction_hash: 'xyzade27abc0ff500eeb31290ed6b1905048845aab5d43359a7e81c0473777400d15',
#                confirmations: 0,
#                sender: 'CATHDLH66eXbdFVVQwAVP2TeJY2pEsGrNt',
#                prev_hash: '57acc6ec2d9967ea8cd4c901c17a840f4cc176265736f7aeec30ff7a65b0e756',
#                value: 1000,
#                btc_cotation: 7989.73,
#                btc_wallet_id: 4)

# puts 'Criando Investimento'
# Investiment.new(user: User.fourth, remuneration_rate: Package.first.remuneration_rate, value: 1000).create_with_balance

# puts 'Ativando Investimento'
# Investiment.last.activate

# puts 'Destruindo Saques'
# BtcWithdraw.destroy_all

# puts 'Criando Saque'
# btc_withdraw = BtcWithdraw.new(user: User.first, value: 100.0, destination: 'CA7HiyiyiCZQhsw5yf9rMPeqL4cr8cF3HQ')
# btc_withdraw.set_attributes
# btc_withdraw.save
# BtcWithdraw.create(user: User.second, amount: 150.0, destination: 'CA7HiyiyiCZQhsw5yf9rMPeqL4cr8cF3HQ', transaction_hash: 'b8bf42b42407e4aee69301ccfa207db79c20fd43c8247cde27a1fa3272eb1bcb')
# BtcWithdraw.create(user: User.third, amount: 200.0, destination: 'CA7HiyiyiCZQhsw5yf9rMPeqL4cr8cF3HQ', transaction_hash: '41523340cdecafe2777d1d880ab812effea4ee13ca0c8164e6d6eee46c6995ac')
# BtcWithdraw.create(user: User.fourth, amount: 50.0, destination: 'CA7HiyiyiCZQhsw5yf9rMPeqL4cr8cF3HQ', transaction_hash: '11ae14213318df561dbefcfa089a33b4cbc724e5dbfa85dd4f0088a8653e9788')


puts 'Destruindo Banks'
  Bank.destroy_all

  puts 'Criando Banks'
  Bank.create([
                { cod: 1, name: 'BB - BANCO DO BRASIL S/A' },
                { cod: 2, name: 'BANCO CENTRAL DO BRASIL' },
                { cod: 3, name: 'BANCO DA AMAZONIA S.A' },
                { cod: 4, name: 'BANCO DO NORDESTE DO BRASIL S.A' },
                { cod: 7, name: 'BANCO NAC DESENV. ECO. SOCIAL S.A' },
                { cod: 8, name: 'BANCO MERIDIONAL DO BRASIL' },
                { cod: 20, name: 'BANCO DO ESTADO DE ALAGOAS S.A' },
                { cod: 21, name: 'BANCO DO ESTADO DO ESPIRITO SANTO S.A' },
                { cod: 22, name: 'BANCO DE CREDITO REAL DE MINAS GERAIS SA' },
                { cod: 24, name: 'BANCO DO ESTADO DE PERNAMBUCO' },
                { cod: 25, name: 'BANCO ALFA S/A' },
                { cod: 26, name: 'BANCO DO ESTADO DO ACRE S.A' },
                { cod: 27, name: 'BANCO DO ESTADO DE SANTA CATARINA S.A' },
                { cod: 28, name: 'BANCO DO ESTADO DA BAHIA S.A' },
                { cod: 29, name: 'BANCO DO ESTADO DO RIO DE JANEIRO S.A' },
                { cod: 30, name: 'BANCO DO ESTADO DA PARAIBA S.A' },
                { cod: 31, name: 'BANCO DO ESTADO DE GOIAS S.A' },
                { cod: 32, name: 'BANCO DO ESTADO DO MATO GROSSO S.A.' },
                { cod: 34, name: 'BANCO DO ESTADO DO AMAZONAS S.A' },
                { cod: 35, name: 'BANCO DO ESTADO DO CEARA S.A' },
                { cod: 36, name: 'BANCO DO ESTADO DO MARANHAO S.A' },
                { cod: 37, name: 'BANCO DO ESTADO DO PARA S.A' },
                { cod: 38, name: 'BANCO DO ESTADO DO PARANA S.A' },
                { cod: 39, name: 'BANCO DO ESTADO DO PIAUI S.A' },
                { cod: 41, name: 'BANCO DO ESTADO DO RIO GRANDE DO SUL S.A' },
                { cod: 47, name: 'BANCO DO ESTADO DE SERGIPE S.A' },
                { cod: 48, name: 'BANCO DO ESTADO DE MINAS GERAIS S.A' },
                { cod: 59, name: 'BANCO DO ESTADO DE RONDONIA S.A' },
                { cod: 70, name: 'BRB - BANCO DE BRASILIA S.A' },
                { cod: 77, name: 'INTER - BANCO INTERMEDIUM S.A.' },
                { cod: 104, name: 'CEF - CAIXA ECONOMICA FEDERAL' },
                { cod: 106, name: 'BANCO ITABANCO S.A.' },
                { cod: 107, name: 'BANCO BBM S.A' },
                { cod: 109, name: 'BANCO CREDIBANCO S.A' },
                { cod: 116, name: 'BANCO B.N.L DO BRASIL S.A' },
                { cod: 148, name: 'MULTI BANCO S.A' },
                { cod: 151, name: 'CAIXA ECONOMICA DO ESTADO DE SAO PAULO' },
                { cod: 153, name: 'CAIXA ECONOMICA DO ESTADO DO R.G.SUL' },
                { cod: 165, name: 'BANCO NORCHEM S.A' },
                { cod: 166, name: 'BANCO INTER-ATLANTICO S.A' },
                { cod: 168, name: 'BANCO C.C.F. BRASIL S.A' },
                { cod: 175, name: 'CONTINENTAL BANCO S.A' },
                { cod: 184, name: 'BBA - CREDITANSTALT S.A' },
                { cod: 199, name: 'BANCO FINANCIAL PORTUGUES' },
                { cod: 200, name: 'BANCO FRICRISA AXELRUD S.A' },
                { cod: 201, name: 'BANCO AUGUSTA INDUSTRIA E COMERCIAL S.A' },
                { cod: 204, name: 'BANCO S.R.L S.A' },
                { cod: 205, name: 'BANCO SUL AMERICA S.A' },
                { cod: 206, name: 'BANCO MARTINELLI S.A' },
                { cod: 208, name: 'BANCO PACTUAL S.A' },
                { cod: 210, name: 'DEUTSCH SUDAMERIKANICHE BANK AG' },
                { cod: 211, name: 'BANCO SISTEMA S.A' },
                { cod: 212, name: 'BANCO MATONE S.A' },
                { cod: 213, name: 'BANCO ARBI S.A' },
                { cod: 214, name: 'BANCO DIBENS S.A' },
                { cod: 215, name: 'BANCO AMERICA DO SUL S.A' },
                { cod: 216, name: 'BANCO REGIONAL MALCON S.A' },
                { cod: 217, name: 'BANCO AGROINVEST S.A' },
                { cod: 218, name: 'BBS - BANCO BONSUCESSO S.A.' },
                { cod: 219, name: 'BANCO DE CREDITO DE SAO PAULO S.A' },
                { cod: 220, name: 'BANCO CREFISUL' },
                { cod: 221, name: 'BANCO GRAPHUS S.A' },
                { cod: 222, name: 'BANCO AGF BRASIL S. A.' },
                { cod: 223, name: 'BANCO INTERUNION S.A' },
                { cod: 224, name: 'BANCO FIBRA S.A' },
                { cod: 225, name: 'BANCO BRASCAN S.A' },
                { cod: 228, name: 'BANCO ICATU S.A' },
                { cod: 229, name: 'BANCO CRUZEIRO S.A' },
                { cod: 230, name: 'BANCO BANDEIRANTES S.A' },
                { cod: 231, name: 'BANCO BOAVISTA S.A' },
                { cod: 232, name: 'BANCO INTERPART S.A' },
                { cod: 233, name: 'BANCO MAPPIN S.A' },
                { cod: 234, name: 'BANCO LAVRA S.A.' },
                { cod: 235, name: 'BANCO LIBERAL S.A' },
                { cod: 236, name: 'BANCO CAMBIAL S.A' },
                { cod: 237, name: 'BANCO BRADESCO S.A' },
                { cod: 239, name: 'BANCO BANCRED S.A' },
                { cod: 240, name: 'BANCO DE CREDITO REAL DE MINAS GERAIS S.' },
                { cod: 241, name: 'BANCO CLASSICO S.A' },
                { cod: 242, name: 'BANCO EUROINVEST S.A' },
                { cod: 243, name: 'BANCO STOCK S.A' },
                { cod: 244, name: 'BANCO CIDADE S.A' },
                { cod: 245, name: 'BANCO EMPRESARIAL S.A' },
                { cod: 246, name: 'BANCO ABC ROMA S.A' },
                { cod: 247, name: 'BANCO OMEGA S.A' },
                { cod: 249, name: 'BANCO INVESTCRED S.A' },
                { cod: 250, name: 'BANCO SCHAHIN CURY S.A' },
                { cod: 251, name: 'BANCO SAO JORGE S.A.' },
                { cod: 252, name: 'BANCO FININVEST S.A' },
                { cod: 254, name: 'BANCO PARANA BANCO S.A' },
                { cod: 255, name: 'MILBANCO S.A.' },
                { cod: 256, name: 'BANCO GULVINVEST S.A' },
                { cod: 258, name: 'BANCO INDUSCRED S.A' },
                { cod: 260, name: 'NU BANK - NU PAGAMENTOS S.A' },
                { cod: 261, name: 'BANCO VARIG S.A' },
                { cod: 262, name: 'BANCO BOREAL S.A' },
                { cod: 263, name: 'BANCO CACIQUE' },
                { cod: 264, name: 'BANCO PERFORMANCE S.A' },
                { cod: 265, name: 'BANCO FATOR S.A' },
                { cod: 266, name: 'BANCO CEDULA S.A' },
                { cod: 267, name: 'BANCO BBM-COM.C.IMOB.CFI S.A.' },
                { cod: 275, name: 'BANCO REAL S.A' },
                { cod: 277, name: 'BANCO PLANIBANC S.A' },
                { cod: 282, name: 'BANCO BRASILEIRO COMERCIAL' },
                { cod: 291, name: 'BANCO DE CREDITO NACIONAL S.A' },
                { cod: 294, name: 'BCR - BANCO DE CREDITO REAL S.A' },
                { cod: 295, name: 'BANCO CREDIPLAN S.A' },
                { cod: 300, name: 'BANCO DE LA NACION ARGENTINA S.A' },
                { cod: 302, name: 'BANCO DO PROGRESSO S.A' },
                { cod: 303, name: 'BANCO HNF S.A.' },
                { cod: 304, name: 'BANCO PONTUAL S.A' },
                { cod: 308, name: 'BANCO COMERCIAL BANCESA S.A.' },
                { cod: 318, name: 'BANCO B.M.G. S.A' },
                { cod: 320, name: 'BANCO INDUSTRIAL E COMERCIAL' },
                { cod: 341, name: 'BANCO ITAU S.A' },
                { cod: 346, name: 'BANCO FRANCES E BRASILEIRO S.A' },
                { cod: 347, name: 'BANCO SUDAMERIS BRASIL S.A' },
                { cod: 351, name: 'BANCO BOZANO SIMONSEN S.A' },
                { cod: 353, name: 'BANCO GERAL DO COMERCIO S.A' },
                { cod: 356, name: 'ABN AMRO S.A' },
                { cod: 366, name: 'BANCO SOGERAL S.A' },
                { cod: 369, name: 'PONTUAL' },
                { cod: 370, name: 'BEAL - BANCO EUROPEU PARA AMERICA LATINA' },
                { cod: 372, name: 'BANCO ITAMARATI S.A' },
                { cod: 375, name: 'BANCO FENICIA S.A' },
                { cod: 376, name: 'CHASE MANHATTAN BANK S.A' },
                { cod: 388, name: 'BANCO MERCANTIL DE DESCONTOS S/A' },
                { cod: 389, name: 'BANCO MERCANTIL DO BRASIL S.A' },
                { cod: 392, name: 'BANCO MERCANTIL DE SAO PAULO S.A' },
                { cod: 394, name: 'BANCO B.M.C. S.A' },
                { cod: 399, name: 'BANCO BAMERINDUS DO BRASIL S.A' },
                { cod: 409, name: 'UNIBANCO - UNIAO DOS BANCOS BRASILEIROS' },
                { cod: 412, name: 'BANCO NACIONAL DA BAHIA S.A' },
                { cod: 415, name: 'BANCO NACIONAL S.A' },
                { cod: 420, name: 'BANCO NACIONAL DO NORTE S.A' },
                { cod: 422, name: 'BANCO SAFRA S.A' },
                { cod: 424, name: 'BANCO NOROESTE S.A' },
                { cod: 434, name: 'BANCO FORTALEZA S.A' },
                { cod: 453, name: 'BANCO RURAL S.A' },
                { cod: 456, name: 'BANCO TOKIO S.A' },
                { cod: 464, name: 'BANCO SUMITOMO BRASILEIRO S.A' },
                { cod: 466, name: 'BANCO MITSUBISHI BRASILEIRO S.A' },
                { cod: 472, name: 'LLOYDS BANK PLC' },
                { cod: 473, name: 'BANCO FINANCIAL PORTUGUES S.A' },
                { cod: 477, name: 'CITIBANK N.A' },
                { cod: 479, name: 'BANCO DE BOSTON S.A' },
                { cod: 480, name: 'BANCO PORTUGUES DO ATLANTICO-BRASIL S.A' },
                { cod: 483, name: 'BANCO AGRIMISA S.A.' },
                { cod: 487, name: 'DEUTSCHE BANK S.A - BANCO ALEMAO' },
                { cod: 488, name: 'BANCO J. P. MORGAN S.A' },
                { cod: 489, name: 'BANESTO BANCO URUGAUAY S.A' },
                { cod: 492, name: 'INTERNATIONALE NEDERLANDEN BANK N.V.' },
                { cod: 493, name: 'BANCO UNION S.A.C.A' },
                { cod: 494, name: 'BANCO LA REP. ORIENTAL DEL URUGUAY' },
                { cod: 495, name: 'BANCO LA PROVINCIA DE BUENOS AIRES' },
                { cod: 496, name: 'BANCO EXTERIOR DE ESPANA S.A' },
                { cod: 498, name: 'CENTRO HISPANO BANCO' },
                { cod: 499, name: 'BANCO IOCHPE S.A' },
                { cod: 501, name: 'BANCO BRASILEIRO IRAQUIANO S.A.' },
                { cod: 33, name: 'BANCO SANTANDER S.A' },
                { cod: 504, name: 'BANCO MULTIPLIC S.A' },
                { cod: 505, name: 'BANCO GARANTIA S.A' },
                { cod: 600, name: 'BANCO LUSO BRASILEIRO S.A' },
                { cod: 601, name: 'BFC BANCO S.A.' },
                { cod: 602, name: 'BANCO PATENTE S.A' },
                { cod: 604, name: 'BANCO INDUSTRIAL DO BRASIL S.A' },
                { cod: 607, name: 'BANCO SANTOS NEVES S.A' },
                { cod: 608, name: 'BANCO OPEN S.A' },
                { cod: 610, name: 'BANCO V.R. S.A' },
                { cod: 611, name: 'BANCO PAULISTA S.A' },
                { cod: 612, name: 'BANCO GUANABARA S.A' },
                { cod: 613, name: 'BANCO PECUNIA S.A' },
                { cod: 616, name: 'BANCO INTERPACIFICO S.A' },
                { cod: 617, name: 'BANCO INVESTOR S.A.' },
                { cod: 618, name: 'BANCO TENDENCIA S.A' },
                { cod: 621, name: 'BANCO APLICAP S.A.' },
                { cod: 622, name: 'BANCO DRACMA S.A' },
                { cod: 623, name: 'BANCO PANAMERICANO S.A' },
                { cod: 624, name: 'BANCO GENERAL MOTORS S.A' },
                { cod: 625, name: 'BANCO ARAUCARIA S.A' },
                { cod: 626, name: 'BANCO FICSA S.A' },
                { cod: 627, name: 'BANCO DESTAK S.A' },
                { cod: 628, name: 'BANCO CRITERIUM S.A' },
                { cod: 629, name: 'BANCORP BANCO COML. E. DE INVESTMENTO' },
                { cod: 630, name: 'BANCO INTERCAP S.A' },
                { cod: 633, name: 'BANCO REDIMENTO S.A' },
                { cod: 634, name: 'BANCO TRIANGULO S.A' },
                { cod: 635, name: 'BANCO DO ESTADO DO AMAPA S.A' },
                { cod: 637, name: 'BANCO SOFISA S.A' },
                { cod: 638, name: 'BANCO PROSPER S.A' },
                { cod: 639, name: 'BIG S.A. - BANCO IRMAOS GUIMARAES' },
                { cod: 640, name: 'BANCO DE CREDITO METROPOLITANO S.A' },
                { cod: 641, name: 'BANCO EXCEL ECONOMICO S/A' },
                { cod: 643, name: 'BANCO SEGMENTO S.A' },
                { cod: 645, name: 'BANCO DO ESTADO DE RORAIMA S.A' },
                { cod: 647, name: 'BANCO MARKA S.A' },
                { cod: 648, name: 'BANCO ATLANTIS S.A' },
                { cod: 649, name: 'BANCO DIMENSAO S.A' },
                { cod: 650, name: 'BANCO PEBB S.A' },
                { cod: 652, name: 'BANCO FRANCES E BRASILEIRO SA' },
                { cod: 653, name: 'BANCO INDUSVAL S.A' },
                { cod: 654, name: 'BANCO A. J. RENNER S.A' },
                { cod: 655, name: 'BANCO VOTORANTIM S.A.' },
                { cod: 656, name: 'BANCO MATRIX S.A' },
                { cod: 657, name: 'BANCO TECNICORP S.A' },
                { cod: 658, name: 'BANCO PORTO REAL S.A' },
                { cod: 702, name: 'BANCO SANTOS S.A' },
                { cod: 705, name: 'BANCO INVESTCORP S.A.' },
                { cod: 707, name: 'BANCO DAYCOVAL S.A' },
                { cod: 711, name: 'BANCO VETOR S.A.' },
                { cod: 713, name: 'BANCO CINDAM S.A' },
                { cod: 715, name: 'BANCO VEGA S.A' },
                { cod: 718, name: 'BANCO OPERADOR S.A' },
                { cod: 719, name: 'BANCO PRIMUS S.A' },
                { cod: 720, name: 'BANCO MAXINVEST S.A' },
                { cod: 721, name: 'BANCO CREDIBEL S.A' },
                { cod: 722, name: 'BANCO INTERIOR DE SAO PAULO S.A' },
                { cod: 724, name: 'BANCO PORTO SEGURO S.A' },
                { cod: 725, name: 'BANCO FINABANCO S.A' },
                { cod: 726, name: 'BANCO UNIVERSAL S.A' },
                { cod: 728, name: 'BANCO FITAL S.A' },
                { cod: 729, name: 'BANCO FONTE S.A' },
                { cod: 730, name: 'BANCO COMERCIAL PARAGUAYO S.A' },
                { cod: 731, name: 'BANCO GNPP S.A.' },
                { cod: 732, name: 'BANCO PREMIER S.A.' },
                { cod: 733, name: 'BANCO NACOES S.A.' },
                { cod: 734, name: 'BANCO GERDAU S.A' },
                { cod: 735, name: 'BACO POTENCIAL' },
                { cod: 736, name: 'BANCO UNITED S.A' },
                { cod: 737, name: 'THECA' },
                { cod: 738, name: 'MARADA' },
                { cod: 739, name: 'BGN' },
                { cod: 740, name: 'BCN BARCLAYS' },
                { cod: 741, name: 'BRP' },
                { cod: 742, name: 'EQUATORIAL' },
                { cod: 743, name: 'BANCO EMBLEMA S.A' },
                { cod: 744, name: 'THE FIRST NATIONAL BANK OF BOSTON' },
                { cod: 745, name: 'CITIBAN N.A.' },
                { cod: 746, name: "MODAL S\A" },
                { cod: 747, name: 'RAIBOBANK DO BRASIL' },
                { cod: 748, name: 'SICREDI' },
                { cod: 749, name: 'BRMSANTIL SA' },
                { cod: 750, name: 'BANCO REPUBLIC NATIONAL OF NEW YORK (BRA' },
                { cod: 751, name: 'DRESDNER BANK LATEINAMERIKA-BRASIL S/A' },
                { cod: 752, name: 'BANCO BANQUE NATIONALE DE PARIS BRASIL S' },
                { cod: 753, name: 'BANCO COMERCIAL URUGUAI S.A.' },
                { cod: 755, name: 'BANCO MERRILL LYNCH S.A' },
                { cod: 756, name: 'BANCO COOPERATIVO DO BRASIL S.A.' },
                { cod: 757, name: 'BANCO KEB DO BRASIL S.A.' }
              ])

              puts 'Criando BankAccountSysAdmin'
              BankAccountSysAdmin.create(bank: Bank.first, agency: 1234, account_number: 0o12345, digit: 8, operation: 1, joint: false, owner_name: 'G44 BRASIL S.A.', cnpj: '28839840000161')
              BankAccountSysAdmin.update_all(status: 'activated')


              puts 'Destruindo BankAccountClient'
              BankAccountClient.destroy_all
            
              puts 'Criando BankAccountClient'

              BankAccountClient.create([
                { owner_name: 'Jorge', cpf: '15234622134', bank_id: 32, agency: 5678, account_number: 910_112, digit: 9, operation: 2, joint: false, account_id: Account.first.id },
                { owner_name: 'Daisy Ridley', cpf: '73676677005', bank_id: 44, agency: 1234, account_number: 0o12345, digit: 8, operation: 1, joint: false, account_id: Account.second.id },
                { owner_name: 'Gal Gadot', cpf: '93205791304', bank_id: 11, agency: 1234, account_number: 0o12345, digit: 8, operation: 1, joint: false, account_id: Account.fourth.id },
                { owner_name: 'Bruna Marquezine', cpf: '62782688928', bank_id: 24, agency: 1234, account_number: 0o12345, digit: 8, operation: 1, joint: false, account_id: Account.fifth.id }
              ])

              BankAccountSysAdmin.create(bank: Bank.first, agency: 1234, account_number: 0o12345, digit: 8, operation: 1, joint: false, owner_name: 'G44 BRASIL S.A.', cnpj: '28839840000161')
              BankAccountSysAdmin.update_all(status: 'activated')